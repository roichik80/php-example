<?php

namespace App\Services\TreeFilter;

use App\Models\Db\AbstractEntityType;

/**
 * Class AbstractHiddenTreeFilterLeaf
 *
 * @package App\Services\TreeFilter
 * @author Aleksandr Roik
 */
abstract class AbstractHiddenTreeFilterLeaf extends AbstractTreeFilterLeaf
{
    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        return true; // Корню всё подходит
    }

    /**
     * Возвращает поддерево в виде ассоциативного массива (для фронта)
     *
     * @return array
     */
    public function toArray(): array
    {
        $array = [
            'title' => $this->title,
            'value' => $this->getHash(),
        ];

        $elements = array_map(function (AbstractTreeFilterLeaf $leaf) {
            return $leaf->toArray();
        }, $this->children);

        if ($elements) {
            $array['elements'] = $elements;
        }

        return $array;
    }

}
