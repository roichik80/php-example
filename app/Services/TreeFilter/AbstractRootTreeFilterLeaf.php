<?php

namespace App\Services\TreeFilter;

use App\Models\Db\AbstractEntityType;
use Illuminate\Database\Eloquent\Builder;

/**
 * Корень дерева фильтров
 * Class AbstractRootTreeFilterLeaf
 *
 * @package App\Services\TreeFilter
 */
class AbstractRootTreeFilterLeaf extends AbstractTreeFilterLeaf
{
    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        return true; // Корню всё подходит
    }

    protected function getMyHash($length = 5): string
    {
        return '';
    }

    protected function isHashFit(string $hash): bool
    {
        return true; // Корню всё подходит
    }

    protected function applyMyCondition(Builder $query): void
    {
        // Do nothing, корень ничего не фильтрует
    }

    public function toArray(): array
    {
        return array_map(function (AbstractTreeFilterLeaf $leaf) {
            return $leaf->toArray();
        }, $this->children);
    }

    public function getTitle()
    {
        return $this->title;
    }
}
