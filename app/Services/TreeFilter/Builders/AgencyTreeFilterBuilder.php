<?php

namespace App\Services\TreeFilter\Builders;

use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use App\Services\TreeFilter\Leafs\BirthdayClientLeaf;
use App\Services\TreeFilter\Leafs\BirthdayEmployeeLead;
use App\Services\TreeFilter\Leafs\ClientLeaf;
use App\Services\TreeFilter\Leafs\DateDealEndsLeaf;
use App\Services\TreeFilter\Leafs\DateEntryReserveRealtyForSaleLeaf;
use App\Services\TreeFilter\Leafs\DatePlannedPriceChangeLeaf;
use App\Services\TreeFilter\Leafs\DateRentContractLeaf;
use App\Services\TreeFilter\Leafs\DateRentEndsLeaf;
use App\Services\TreeFilter\Leafs\DateReservationEndsLeaf;
use App\Services\TreeFilter\Leafs\DateShareEndsLeaf;
use App\Services\TreeFilter\Leafs\DateTechnicalReservationEndsLeaf;
use App\Services\TreeFilter\Leafs\DialogLeaf;
use App\Services\TreeFilter\Leafs\LeadLeaf;
use App\Services\TreeFilter\Leafs\LegalEntityLeaf;
use App\Services\TreeFilter\Leafs\MeetingLeaf;
use App\Services\TreeFilter\Leafs\NaturalPersonLeaf;
use App\Services\TreeFilter\Leafs\NotificationRealtyStatusPrimaryLeaf;
use App\Services\TreeFilter\Leafs\OmniChartLeaf;
use App\Services\TreeFilter\Leafs\OrderDemandLeaf;
use App\Services\TreeFilter\Leafs\OrderRentLeaf;
use App\Services\TreeFilter\Leafs\PhoneLeaf;
use App\Services\TreeFilter\Leafs\RealtyRentTreeFilterLeaf;
use App\Services\TreeFilter\Leafs\RealtySecondaryTreeFilterLeaf;
use App\Services\TreeFilter\Leafs\RootHiddenTreeFilterLeaf;
use App\Services\TreeFilter\Leafs\RootPublicTreeFilterLeaf;
use App\Services\TreeFilter\Leafs\RootTreeFilterLeaf;
use App\Services\TreeFilter\Leafs\ServiceContractEndsLeaf;
use App\Services\TreeFilter\Leafs\ServiceLeaf;
use App\Services\TreeFilter\Leafs\SetAsideLeaf;
use App\Services\TreeFilter\Leafs\TaskLeaf;
use App\Services\TreeFilter\Leafs\TaskOnContractLeaf;
use App\Services\TreeFilter\Leafs\TaskOnDealLeaf;
use App\Services\TreeFilter\Leafs\TaskOnRentLeaf;
use App\Services\TreeFilter\Leafs\TaskOnSecondaryLeaf;
use App\Services\TreeFilter\Leafs\TaskOnStatusLeaf;
use App\Services\TreeFilter\Leafs\TaskOnStatusWithRemindersLeaf;
use App\Services\TreeFilter\Leafs\TaskOverdueFilterLeaf;

/**
 * Строит фильтр для типа компании "Агенства"
 * Class AgencyTreeFilterBuilder
 *
 * @package App\Services\TreeFilter\Builders
 * @author Aleksandr Roik
 */
class AgencyTreeFilterBuilder extends AbstractTreeBuilder
{
    /**
     * Cтроим структуру фильтра
     *
     * @return AbstractTreeFilterLeaf
     */
    public function build(): AbstractTreeFilterLeaf
    {
        $rootTree = new RootTreeFilterLeaf();

        $rootTree->setChildren([
            (new RootHiddenTreeFilterLeaf())->setChildren([
                new TaskOverdueFilterLeaf(),
            ]),
            (new RootPublicTreeFilterLeaf())->setChildren([
                (new RealtyRentTreeFilterLeaf())
                    ->setChildren([
                        new TaskLeaf(),
                        new TaskOnDealLeaf(),
                        new TaskOnContractLeaf(),
                        (new TaskOnStatusWithRemindersLeaf('Задачи по статусам объектов аренды'))
                            ->setChildren([
                                new DateRentEndsLeaf(),
                                new SetAsideLeaf(),
                                new DateRentContractLeaf(),
                            ]),
                        new ServiceContractEndsLeaf(),
                    ]),
                (new RealtySecondaryTreeFilterLeaf())
                    ->setChildren([
                        new TaskLeaf(),
                        new TaskOnDealLeaf(),
                        new TaskOnContractLeaf(),
                        (new TaskOnStatusLeaf('Задачи по статусам объектов вторички'))
                            ->setChildren([
                                new SetAsideLeaf(),
                                new DateRentContractLeaf(),
                            ]),
                        new ServiceContractEndsLeaf(),
                    ]),
                (new OrderRentLeaf())
                    ->setChildren([
                        new TaskLeaf(),
                        new TaskOnDealLeaf(),
                        new TaskOnContractLeaf(),
                        (new TaskOnStatusWithRemindersLeaf('Задачи по статусам заявок по аренде'))
                            ->setChildren([
                                new DateRentEndsLeaf(),
                                new SetAsideLeaf(),
                                new DateRentContractLeaf(),
                            ]),
                        new ServiceContractEndsLeaf(),
                    ]),
                (new OrderDemandLeaf())
                    ->setChildren([
                        new TaskLeaf(),
                        new TaskOnDealLeaf(),
                        new TaskOnContractLeaf(),
                        (new TaskOnStatusLeaf('Задачи по статусам заявок на покупку'))
                            ->setChildren([
                                new SetAsideLeaf(),
                                new DateRentContractLeaf(),
                            ]),
                        new ServiceContractEndsLeaf(),
                    ]),
                new ServiceLeaf(),
                (new LeadLeaf())
                    ->setChildren([
                        new TaskOnRentLeaf(),
                        new TaskOnSecondaryLeaf(),
                    ]),
                (new ClientLeaf())
                    ->setChildren([
                        new LegalEntityLeaf(),
                        new NaturalPersonLeaf(),
                    ]),
                (new DialogLeaf())
                    ->setChildren([
                        new PhoneLeaf(),
                        new OmniChartLeaf(),
                    ]),
                new BirthdayClientLeaf(),
                new BirthdayEmployeeLead(),
                (new NotificationRealtyStatusPrimaryLeaf())
                    ->setChildren([
                        new DateTechnicalReservationEndsLeaf(),
                        new DateReservationEndsLeaf(),
                        new DateDealEndsLeaf(),
                        new DateEntryReserveRealtyForSaleLeaf(),
                    ]),
                new DatePlannedPriceChangeLeaf(),
                new DateShareEndsLeaf(),
                new MeetingLeaf(),
            ]),
        ]);

        return $rootTree;
    }
}

