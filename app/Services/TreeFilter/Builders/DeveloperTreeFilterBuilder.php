<?php

namespace App\Services\TreeFilter\Builders;

use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use App\Services\TreeFilter\Leafs\BirthdayClientLeaf;
use App\Services\TreeFilter\Leafs\BirthdayEmployeeLead;
use App\Services\TreeFilter\Leafs\ClientLeaf;
use App\Services\TreeFilter\Leafs\DateDealEndsLeaf;
use App\Services\TreeFilter\Leafs\DateEntryReserveRealtyForSaleLeaf;
use App\Services\TreeFilter\Leafs\DatePlannedPriceChangeLeaf;
use App\Services\TreeFilter\Leafs\DateReservationEndsLeaf;
use App\Services\TreeFilter\Leafs\DateShareEndsLeaf;
use App\Services\TreeFilter\Leafs\DateTechnicalReservationEndsLeaf;
use App\Services\TreeFilter\Leafs\DialogLeaf;
use App\Services\TreeFilter\Leafs\LegalEntityLeaf;
use App\Services\TreeFilter\Leafs\MeetingLeaf;
use App\Services\TreeFilter\Leafs\NaturalPersonLeaf;
use App\Services\TreeFilter\Leafs\NotificationRealtyStatusLeaf;
use App\Services\TreeFilter\Leafs\OmniChartLeaf;
use App\Services\TreeFilter\Leafs\OrderDemandLeaf;
use App\Services\TreeFilter\Leafs\PhoneLeaf;
use App\Services\TreeFilter\Leafs\RootHiddenTreeFilterLeaf;
use App\Services\TreeFilter\Leafs\RootTreeFilterLeaf;
use App\Services\TreeFilter\Leafs\TaskFromDeveloper;
use App\Services\TreeFilter\Leafs\TaskOnDealLeaf;
use App\Services\TreeFilter\Leafs\TaskOnLeadLeaf;
use App\Services\TreeFilter\Leafs\TaskOnSetAsideLeaf;
use App\Services\TreeFilter\Leafs\TaskOverdueFilterLeaf;

/**
 * Строит фильтр для типа компании "Застройщики"
 * Class DeveloperTreeFilterBuilder
 *
 * @package App\Services\TreeFilter\Builders
 * @author Aleksandr Roik
 */
class DeveloperTreeFilterBuilder extends AbstractTreeBuilder
{
    /**
     * Cтроим структуру фильтра
     *
     * @return AbstractTreeFilterLeaf
     */
    public function build(): AbstractTreeFilterLeaf
    {
        $rootTree = new RootTreeFilterLeaf();

        $rootTree
            ->setChildren([
                (new RootHiddenTreeFilterLeaf())->setChildren([
                    new TaskOverdueFilterLeaf(),
                ]),
                (new RootHiddenTreeFilterLeaf())->setChildren([
                    (new OrderDemandLeaf())
                        ->setChildren([
                            new TaskFromDeveloper(),
                            new TaskOnDealLeaf(),
                            new TaskOnSetAsideLeaf(),
                        ]),
                    new TaskOnLeadLeaf(),
                    (new ClientLeaf())
                        ->setChildren([
                            new LegalEntityLeaf(),
                            new NaturalPersonLeaf(),
                        ]),
                    (new DialogLeaf())
                        ->setChildren([
                            new PhoneLeaf(),
                            new OmniChartLeaf(),
                        ]),
                    new BirthdayClientLeaf(),
                    new BirthdayEmployeeLead(),
                    (new NotificationRealtyStatusLeaf())
                        ->setChildren([
                            new DateTechnicalReservationEndsLeaf(),
                            new DateReservationEndsLeaf(),
                            new DateDealEndsLeaf(),
                            new DateEntryReserveRealtyForSaleLeaf(),
                        ]),
                    new DatePlannedPriceChangeLeaf(),
                    new DateShareEndsLeaf(),
                    new MeetingLeaf(),
                ])
            ]);

        return $rootTree;
    }
}

