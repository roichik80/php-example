<?php

namespace App\Services\TreeFilter\Builders;

use App\Services\TreeFilter\AbstractTreeFilterLeaf;

/**
 * Строит фильтр для скрытого использования, к примеру фронтом
 * Class PrivateTreeBuilder
 *
 * @package App\Services\TreeFilter\Builders
 * @author Aleksandr Roik
 */
class PrivateTreeBuilder extends AbstractTreeBuilder
{
    /**
     * Cтроим структуру фильтра
     *
     * @return AbstractTreeFilterLeaf
     */
    public function build(): AbstractTreeFilterLeaf
    {

        $rootTree = new RootTreeFilterLeaf();


        return $rootTree;
    }
}
