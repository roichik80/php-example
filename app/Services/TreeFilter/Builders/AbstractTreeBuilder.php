<?php

namespace App\Services\TreeFilter\Builders;

use App\Services\TreeFilter\AbstractTreeFilterLeaf;

abstract class AbstractTreeBuilder
{
    /**
     * @var AbstractTreeFilterLeaf
     */
    protected $tree;

    /**
     * @return AbstractTreeFilterLeaf
     */
    abstract public function build(): AbstractTreeFilterLeaf;
}
