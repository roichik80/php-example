<?php

namespace App\Services\TreeFilter;

use App\Models\Db\AbstractEntityType;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class AbstractTreeFilterLeaf
 *
 * @package App\Services\TreeFilter
 */
abstract class AbstractTreeFilterLeaf
{
    /** @var AbstractTreeFilterLeaf */
    protected $parent;

    /** @var AbstractTreeFilterLeaf[] */
    protected $children = [];

    /**
     * Подпись листа
     *
     * @var string|null
     */
    protected $title;

    public function __construct(string $title = null)
    {
        if ($title !== null) {
            $this->title = $title;
        }
    }

    /**
     * Должен возвращать, подходит ли сущность этому ($this) листу
     *
     * @param AbstractEntityType $entityType
     * @return bool
     */
    abstract protected function isEntityFit(AbstractEntityType $entityType): bool;

    /**
     * Должен применять к запросу свои условия выборки (без родительских/дочерних)
     *
     * @param Builder $query
     */
    abstract protected function applyMyCondition(Builder $query): void;

    /**
     * Возвращает хєш текущего листа
     *
     * @param int $length
     * @return string
     */
    protected function getMyHash($length = 5): string
    {
        return mb_strtoupper(substr(md5(static::class), 0, $length));
    }

    /**
     * Возвращает, подходит ли хеш этому ($this) листу
     * Внимание: этот метод можно переопределять, но нужно следить за длиной "подхеша"
     *
     * @param string $hash
     * @return bool
     */
    protected function isHashFit(string $hash): bool
    {
        return strpos($hash, $this->getMyHash()) === 0;
    }

    /**
     * Возвращает поддерево в виде ассоциативного массива (для фронта)
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'title'    => $this->title,
            'value'    => $this->getHash(),
            'elements' => array_map(function (AbstractTreeFilterLeaf $leaf) {
                return $leaf->toArray();
            }, $this->children)
        ];
    }

    /**
     * Задать родителя
     *
     * @param AbstractTreeFilterLeaf $parent
     * @return self
     */
    final public function setParent(AbstractTreeFilterLeaf $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Задать список детей
     *
     * @param AbstractTreeFilterLeaf[] $leafs
     * @return self
     */
    final public function setChildren(array $leafs): self
    {
        foreach ($leafs as $leaf) {
            $leaf->setParent($this);
        }

        $this->children = $leafs;

        return $this;
    }

    /**
     * Должен возвращать массив непосредственно-дочерних листьев
     * Можно переопределять, например, чтобы взять детей из базы
     *
     * @return AbstractTreeFilterLeaf[]
     */
    protected function getChildren(): array
    {
        return $this->children;
    }

    /**
     * Найти лист, максимально подходищий переданной сущности
     *
     * @param AbstractEntityType $entityType
     * @return self
     */
    final public function findByEntity(AbstractEntityType $entityType): ?AbstractTreeFilterLeaf
    {
        if (!$this->isEntityFit($entityType)) {
            return null;
        }

        $found = null;
        foreach ($this->getChildren() as $child) {
            $found = $child->findByEntity($entityType);
            if ($found) {
                return $found;
            }
        }

        return $this;
    }

    /**
     * Найти лист, максимально подходящий переданному хешу
     *
     * @param string $hash
     * @return self|null
     */
    final public function findByHash(string $hash): ?AbstractTreeFilterLeaf
    {
        // Не хардкодим сравнение переданного хеша с собственным, чтобы можно было переопределить
        if (!$this->isHashFit($hash)) {
            return null;
        }

        $myHash = $this->getMyHash();

        // Вырезаем "свой" хеш с начала строки
        $hash = substr_replace($hash, '', 0, strlen($myHash));

        if ($hash === '') {
            return $this;
        }

        // Ищем более подходящий лист среди детей
        $found = null;
        foreach ($this->getChildren() as $child) {
            $found = $child->findByHash($hash);
            if ($found) {
                return $found;
            }
        }

        // Среди детей ничего не подошло, возвращаем текущий
        return $this;
    }

    /**
     * Получить полный хеш (с префиксами родителей)
     *
     * @return string
     */
    final public function getHash(): string
    {
        return $this->getParentHash() . $this->getMyHash();
    }

    /**
     * Получить список всех хешей, в т.ч. хешей дочерних классов
     *
     * @return array
     */
    final public function getHashes(): array
    {
        $hash = $this->getHash();
        if (!$hash) {
            $hash = [];
        }

        return array_merge((array)$hash, $this->getChildrenHashes());
    }

    /**
     * Получить список всех хешей дочерних классов
     *
     * @return array
     */
    final public function getChildrenHashes(): array
    {
        $hashes = [];

        foreach ($this->getChildren() as $child) {
            $hashes = array_merge($hashes, $child->getHashes());
        }

        return $hashes;
    }

    // Применить условия фильтрации (включая родительские)
    final public function applyConditions(Builder $query): void
    {
        $this->applyMyCondition($query);
    }

    /**
     * Возвращает хеш родителя
     *
     * @return string
     */
    final protected function getParentHash(): ?string
    {
        if (isset($this->parent)) {
            return $this->parent->getHash();
        }

        return null;
    }
}
