<?php

namespace App\Services\TreeFilter;

/**
 * Class AbstractCustomTreeFilterLeaf
 *
 * @package App\Services\TreeFilter
 * @author Aleksandr Roik
 */
abstract class AbstractCustomTreeFilterLeaf extends AbstractTreeFilterLeaf
{
    /**
     * Хєш
     *
     * @var string|null
     */
    protected $myHash = '';

    /**
     * Устанавливает хєш для текущего листа
     *
     * @param $myHash
     * @return AbstractTreeFilterLeaf
     */
    public function setMyHash($myHash): self
    {
        $this->myHash = $myHash;

        return $this;
    }

    protected function getMyHash($length = 5): string
    {
        return $this->myHash;
    }
}
