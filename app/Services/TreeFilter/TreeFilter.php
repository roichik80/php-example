<?php

namespace App\Services\TreeFilter;

use App\Models\Db\AbstractEntityType;
use App\Services\TreeFilter\Builders\AbstractTreeBuilder;
use App\Services\TreeFilter\Builders\AgencyTreeFilterBuilder;
use App\Services\TreeFilter\Builders\DeveloperTreeFilterBuilder;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Users\CompanyAccountsTypesDefinition;

/**
 * Class TreeFilter
 *
 * @package App\Services\TreeFilter
 * @author Aleksandr Roik
 */
class TreeFilter
{
    protected $tree = [];

    /**
     * Строит дерево фильтров
     *
     * @param int $companyType
     */
    protected function build(int $companyType): AbstractTreeFilterLeaf
    {
        $treeFilterBulder = $this->getTreeFilterBuilderByCompanyType($companyType);
        $this->tree[$companyType] = $treeFilterBulder->build();

        return $this->tree[$companyType];
    }

    /**
     * Возвращает конструктор дерева фильров
     *
     * @return AbstractTreeBuilder|null
     */
    protected function getTreeFilterBuilderByCompanyType(int $companyType): ?AbstractTreeBuilder
    {
        switch ($companyType) {
            case CompanyAccountsTypesDefinition::TYPE_AGENCY:
                return new AgencyTreeFilterBuilder();
            case CompanyAccountsTypesDefinition::TYPE_DEVELOPER:
                return new DeveloperTreeFilterBuilder();
        }

        return null;
    }

    /**
     * Возвращает дерево фильтров
     *
     * @param int $companyType
     * @return AbstractTreeFilterLeaf
     */
    protected function getTree(int $companyType): AbstractTreeFilterLeaf
    {
        return $this->tree[$companyType] ?? $this->build($companyType);
    }

    /**
     * Получить "хеш" фильтра
     *
     * @param int $companyType
     * @param AbstractEntityType $entityType
     * @return string|null
     */
    public function getFilterHash(int $companyType, AbstractEntityType $entityType): string
    {
        $treeLeaf = $this
            ->getTree($companyType)
            ->findByEntity($entityType);

        return $treeLeaf ? $treeLeaf->getHash() : '';
    }

    /**
     * Получить "хеши" фильтра
     *
     * @param int $companyType
     * @return array
     */
    public function getFilterHashes(int $companyType): array
    {
        $treeLeaf = $this
            ->getTree($companyType)
            ->getHashes();

        return $treeLeaf;
    }

    /**
     * Получить массив с деревом фильтров (для фронта)
     *
     * @return array
     */
    public function getFilterTree(int $companyType): array
    {
        return $this
            ->getTree($companyType)
            ->toArray();
    }

    /**
     * Применить к запросу условия фильтрации
     *
     * @param int $companyType
     * @param string $hash
     * @param Builder $query
     */
    public function applyConditions(int $companyType, string $hash, Builder $query): void
    {
        $treeLeaf = $this
            ->getTree($companyType)
            ->findByHash($hash);

        if ($treeLeaf) {
            $treeLeaf->applyConditions($query);
        }
    }
}
