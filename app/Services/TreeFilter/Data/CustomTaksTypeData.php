<?php

namespace App\Services\TreeFilter\Data;

use App\Facades\User;
use App\Managers\CustomTaskTypeManager;

/**
 * Возвращает массивы данных типов задач. Используется кэширование
 * Class CustomTaksTypeData
 *
 * @package App\Services\TreeFilter\Data
 * @author Aleksandr Roik
 */
class CustomTaksTypeData
{
    /**
     * Методы
     */
    const GET_CUSTOM_TASK_TYPE_ALL = 1;
    const GET_CUSTOM_TASK_TYPE_ONLY_FLAG = 2;
    const GET_CUSTOM_TASK_TYPE_WITHOUT_FLAG = 3;

    /**
     * Менеджер
     *
     * @var CustomTaskTypeManager::class
     */
    private $manager;

    /**
     * Список закешированных данных
     *
     * @var array
     */
    static private $catch = [];

    /**
     * CustomTaksTypeData constructor.
     */
    public function __construct()
    {
        $this->manager = CustomTaskTypeManager::class;
    }

    /**
     * Возвращает коллекцию всех типов задач компании
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getCustomTaskTypeAll()
    {
        if (array_key_exists(self::GET_CUSTOM_TASK_TYPE_ALL, self::$catch)) {
            return self::$catch[self::GET_CUSTOM_TASK_TYPE_ALL];
        }

        $collection = (new $this->manager())
            ->byAttributes(User::getCompanyId(), [])
            ->get();

        self::$catch[self::GET_CUSTOM_TASK_TYPE_ALL] = $collection;

        return $collection;
    }

    /**
     * Возвращает коллекцию типов задач определенного флага(ов)
     *
     * @param string|array $flags
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getCustomTaskTypeOnlyFlags($flags)
    {
        if (array_key_exists(self::GET_CUSTOM_TASK_TYPE_ONLY_FLAG, self::$catch)) {
            return self::$catch[self::GET_CUSTOM_TASK_TYPE_ONLY_FLAG];
        }

        $collection = (new $this->manager())->findOnlyFlags($flags);
        self::$catch[self::GET_CUSTOM_TASK_TYPE_ONLY_FLAG] = $collection;

        return $collection;
    }

    /**
     * Возвращает коллекцию типов задач без определенного флага(ов)
     *
     * @param string|array $flags
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getCustomTaskTypeWithOutFlags($flags)
    {
        if (array_key_exists(self::GET_CUSTOM_TASK_TYPE_WITHOUT_FLAG, self::$catch)) {
            return self::$catch[self::GET_CUSTOM_TASK_TYPE_WITHOUT_FLAG];
        }

        $collection = (new $this->manager())->findWithOutFlags($flags);
        self::$catch[self::GET_CUSTOM_TASK_TYPE_WITHOUT_FLAG] = $collection;

        return $collection;
    }
}
