<?php

namespace App\Services\TreeFilter;

use App\Services\TreeFilter\Data\CustomTaksTypeData;
use App\Services\TreeFilter\Leafs\CustomTaskTypeLeaf;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class AbstractTaskTreeLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
abstract class AbstractTaskTreeLeaf extends AbstractTreeFilterLeaf
{
    public function __construct(string $title = null)
    {
        parent::__construct($title);

        $collection = $this->getCustomTaskTypeCollection(resolve(CustomTaksTypeData::class));
        $this->setChildrenByCollection($collection);
    }

    /**
     * Должна возвращать коллекцию сущностей из модели
     *
     * @return mixed
     */
    abstract protected function getCustomTaskTypeCollection(CustomTaksTypeData $customTaksTypeData): Collection;

    /**
     * Задает список детей по данных колекции Типов задач
     *
     * @param Collection $customTaskTypeCollection
     * @return $this
     */
    private function setChildrenByCollection(Collection $customTaskTypeCollection)
    {
        if (!$customTaskTypeCollection || $customTaskTypeCollection->isEmpty()) {
            return $this;
        }

        $leafs = [];

        foreach ($customTaskTypeCollection as $collection) {
            $leaf = new CustomTaskTypeLeaf($collection->title);
            $leaf
                ->setMyHash($collection->id);

            $leafs[] = $leaf;
        }

        $this->setChildren($leafs);

        return $this;
    }
}
