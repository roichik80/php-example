<?php

namespace App\Services\TreeFilter\Leafs;

use App\Services\TreeFilter\AbstractRootTreeFilterLeaf;

/**
 * Корень дерева фильтров
 * Class RootTreeFilterLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 */
class RootTreeFilterLeaf extends AbstractRootTreeFilterLeaf
{
    public function toArray(): array
    {
        $result = [];

        foreach ($this->children as $child){
            $result[$child->getTitle()] = $child->toArray();
        }

        return $result;
    }
}
