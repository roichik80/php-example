<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Reminder;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\ReminderSubtypeDefinition;

/**
 * Дни рождения клиентов
 * Class BirthdayClientLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class BirthdayClientLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Дни рождения клиентов';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (
            $entityType instanceof Reminder &&
            $entityType->subtype == ReminderSubtypeDefinition::BIRTHDAY_CLIENT
        ) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', $this->getHash());
    }

    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'value' => $this->getHash(),
        ];
    }
}
