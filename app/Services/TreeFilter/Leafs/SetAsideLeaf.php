<?php

namespace App\Services\TreeFilter\Leafs;

use App\Definitions\ObjectStatusDefinition;
use App\Models\Db\AbstractEntityType;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\TaskSubtypeDefinition;

/**
 * Задачи по статусам: Отложенные объекты
 * Class SetAsideLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class SetAsideLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Отложенные объекты';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (
            $entityType instanceof Task &&
            $entityType->subtype == TaskSubtypeDefinition::TASK_ON_STATUS &&
            $entityType->object_status = ObjectStatusDefinition::SET_ASIDE
        ) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', $this->getHash());
    }

    public function toArray(): array
    {
        return [
            'title'    => $this->title,
            'value'    => $this->getHash(),
        ];
    }
}
