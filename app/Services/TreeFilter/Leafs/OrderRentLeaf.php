<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Reminder;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\OwnerTypeDefinition;
use Topnlab\Common\v2\Reference\Secondary\Realty\RealtyActionTypeDefinition;

/**
 * Заявки по аренде
 * Class OrderRentLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class OrderRentLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Заявки по аренде';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (
            (
                $entityType instanceof Task &&
                $entityType->owner_type == OwnerTypeDefinition::AGENCY_CLIENT_ORDER &&
                $entityType->object_action == RealtyActionTypeDefinition::REALTY_ACTION_TYPE_RENT
            )
            ||
            (
                $entityType instanceof Reminder &&
                $entityType->owner_type == OwnerTypeDefinition::AGENCY_CLIENT_ORDER &&
                $entityType->object_action == RealtyActionTypeDefinition::REALTY_ACTION_TYPE_RENT
            )
        ) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', 'like', $this->getHash() . '%');
    }
}
