<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Reminder;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\ReminderSubtypeDefinition;
use Topnlab\Common\v2\Reference\Calendar\TaskSubtypeDefinition;

/**
 * Задачи по статусам (с напоминаниями)
 * Class TaskOnStatusWithRemindersLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class TaskOnStatusWithRemindersLeaf extends AbstractTreeFilterLeaf
{
    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (
            ($entityType instanceof Task &&
                (
                    $entityType->subtype == TaskSubtypeDefinition::TASK_ON_STATUS ||
                    $entityType->subtype == TaskSubtypeDefinition::RENT_CONTRACT
                )
            )
            ||
            ($entityType instanceof Reminder && $entityType->subtype == ReminderSubtypeDefinition::RENT_ENDS)
        ) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', 'like', $this->getHash() . '%');
    }
}
