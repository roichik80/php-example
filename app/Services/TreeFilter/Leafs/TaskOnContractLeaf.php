<?php

namespace App\Services\TreeFilter\Leafs;

use App\Definitions\CustomTaskTypeFlagDefinition;
use App\Models\Db\AbstractEntityType;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTaskTreeLeaf;
use App\Services\TreeFilter\Data\CustomTaksTypeData;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Topnlab\Common\v2\Reference\Calendar\TaskSubtypeDefinition;

/**
 * Задачи по договорам
 * Class TaskOnContractLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class TaskOnContractLeaf extends AbstractTaskTreeLeaf
{
    protected $title = 'Задачи по договорам';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (($entityType instanceof Task) && $entityType->subtype == TaskSubtypeDefinition::TASK_ON_CONTRACT) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', 'like', $this->getHash() . '%');
    }

    protected function getCustomTaskTypeCollection(CustomTaksTypeData $customTaksTypeData): Collection
    {
        return $customTaksTypeData->getCustomTaskTypeOnlyFlags(CustomTaskTypeFlagDefinition::TEXT_NODE);
    }
}
