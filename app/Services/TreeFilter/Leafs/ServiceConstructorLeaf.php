<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Services\TreeFilter\AbstractCustomTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;

/**
 * Вид услуги (конструктор)
 * Class ServiceConstructorLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class ServiceConstructorLeaf extends AbstractCustomTreeFilterLeaf
{
    protected $title = 'Вид услуги (конструктор)';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        return true;
    }

    protected function applyMyCondition(Builder $query): void
    {

    }
}
