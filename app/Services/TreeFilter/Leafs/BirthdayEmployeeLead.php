<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Reminder;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\ReminderSubtypeDefinition;

/**
 * Дни рождения коллег
 * Class BirthdayEmployeeLead
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class BirthdayEmployeeLead extends AbstractTreeFilterLeaf
{
    protected $title = 'Дни рождения коллег';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (
            $entityType instanceof Reminder &&
            $entityType->subtype == ReminderSubtypeDefinition::BIRTHDAY_EMPLOYEE
        ) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', $this->getHash());
    }

    public function toArray(): array
    {
        return [
            'title'    => $this->title,
            'value'    => $this->getHash(),
        ];
    }
}
