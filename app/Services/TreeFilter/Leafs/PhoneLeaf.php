<?php

namespace App\Services\TreeFilter\Leafs;

use App\Definitions\CustomTaskTypeFlagDefinition;
use App\Models\Db\AbstractEntityType;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTaskTreeLeaf;
use App\Services\TreeFilter\Data\CustomTaksTypeData;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Topnlab\Common\v2\Reference\Calendar\TaskSubtypeDefinition;

/**
 * Телефония
 * Class PhoneLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class PhoneLeaf extends AbstractTaskTreeLeaf
{
    protected $title = 'Телефония';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if ($entityType instanceof Task && $entityType->subtype == TaskSubtypeDefinition::PHONE) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', 'like', $this->getHash() . '%');
    }

    protected function getCustomTaskTypeCollection(CustomTaksTypeData $customTaksTypeData): Collection
    {
        return $customTaksTypeData->getCustomTaskTypeOnlyFlags(CustomTaskTypeFlagDefinition::TEXT_NODE);
    }
}
