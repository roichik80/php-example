<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Reminder;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\OwnerTypeDefinition;

/**
 * Напоминания по статусам объектов первички
 * Class NotificationRealtyStatusPrimaryLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class NotificationRealtyStatusPrimaryLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Напоминания по статусам объектов первички';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (
            $entityType instanceof Reminder &&
            $entityType->owner_type == OwnerTypeDefinition::PRIMARY_REALTY
        ) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', 'like', $this->getHash() . '%');
    }
}
