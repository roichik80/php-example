<?php

namespace App\Services\TreeFilter\Leafs;

use App\Definitions\CustomTaskTypeFlagDefinition;
use App\Models\Db\AbstractEntityType;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTaskTreeLeaf;
use App\Services\TreeFilter\Data\CustomTaksTypeData;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Topnlab\Common\v2\Reference\Calendar\TaskSubtypeDefinition;

/**
 * Физическое лицо
 * Class NaturalPersonLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class NaturalPersonLeaf extends AbstractTaskTreeLeaf
{
    protected $title = 'Физическое лицо';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if ($entityType instanceof Task && $entityType->subtype == TaskSubtypeDefinition::NATURAL_PERSON) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', 'like', $this->getHash() . '%');
    }

    protected function getCustomTaskTypeCollection(CustomTaksTypeData $customTaksTypeData): Collection
    {
        return $customTaksTypeData->getCustomTaskTypeOnlyFlags(CustomTaskTypeFlagDefinition::TEXT_NODE);
    }
}
