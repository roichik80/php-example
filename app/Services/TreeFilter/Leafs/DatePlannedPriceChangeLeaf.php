<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Reminder;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\ReminderSubtypeDefinition;

/**
 * Дата запланированных изменений цен
 * Class DatePlannedPriceChangeLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class DatePlannedPriceChangeLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Дата запланированных изменений цен';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (
            $entityType instanceof Reminder &&
            $entityType->subtype == ReminderSubtypeDefinition::PLANNED_PRICE_CHANGE
        ) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', $this->getHash());
    }

    public function toArray(): array
    {
        return [
            'title'    => $this->title,
            'value'    => $this->getHash(),
        ];
    }
}
