<?php

namespace App\Services\TreeFilter\Leafs;

use App\Services\TreeFilter\AbstractRootTreeFilterLeaf;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;

/**
 * Корень дерева публичных фильтров
 * Class RootPublicTreeFilterLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 */
class RootPublicTreeFilterLeaf extends AbstractRootTreeFilterLeaf
{
    protected $title = 'public';

    public function toArray(): array
    {
        return array_map(function (AbstractTreeFilterLeaf $leaf) {
            return $leaf->toArray();
        }, $this->children);
    }
}
