<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\TaskSubtypeDefinition;

/**
 * Задачи по статусам: Отложенные объекты
 * Class TaskOnSetAsideLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class TaskOnSetAsideLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Задачи по отлеженным заявкам';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (($entityType instanceof Task) && $entityType->subtype == TaskSubtypeDefinition::TASK) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', $this->getHash());
    }
}
