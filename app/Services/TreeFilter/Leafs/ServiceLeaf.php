<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Topnlab\Common\v2\Reference\Calendar\OwnerTypeDefinition;

/**
 * Услуги
 * Class ServiceLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class ServiceLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Услуги';

    public function __construct(string $title = null)
    {
        parent::__construct($title);

        $this->setChildrenByCollection();
    }

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (
            $entityType instanceof Task &&
            $entityType->owner_type == OwnerTypeDefinition::AGENCY_CLIENT_ORDER &&
            $this->isService($entityType)
        ) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', 'like', $this->getHash() . '%');
    }

    /**
     * Проверяет сущность и возвращает статус пренадлежности к "Услуге"
     *
     * @param AbstractEntityType $entityType
     * @return bool
     */
    private function isService(AbstractEntityType $entityType): bool
    {
        //TODO: Пока заглушка! Надо написать проверку: обратиться к ядру, запросить заявку и проверить поле service
        return false;
    }

    /**
     * Возвращает список услуг
     *
     * @return Collection
     */
    private function getServiceDataList(): Collection
    {
        //TODO: Пока заглушка! Данные будут браться с \Topnlab\Common\v2\Reference\Secondary\ClientOrder\ClientOrderTypeOfServiceDefinition + Конструктор
        $serviceDataList = collect([
            [
                'id'    => 1,
                'title' => 'Услуга 1',
            ],
            [
                'id'    => 2,
                'title' => 'Услуга 2',
            ],
        ]);

        return $serviceDataList;
    }

    /**
     * Задает список детей по данных колекции Услуг
     *
     * @return $this
     */
    private function setChildrenByCollection()
    {
        $serviceDataList = $this->getServiceDataList();

        if (!$serviceDataList || $serviceDataList->isEmpty()) {
            return $this;
        }

        $leafs = [];

        foreach ($serviceDataList as $collection) {
            $leaf = new ServiceConstructorLeaf($collection['title']);
            $leaf
                ->setMyHash($collection['id'])
                ->setChildren([
                    new TaskFromServiceLeaf(),
                    new TaskOnDealLeaf(),
                    new TaskOnContractLeaf(),
                    (new TaskOnStatusLeaf('Задачи по статусам заявок на услуги'))
                        ->setChildren([
                            new SetAsideLeaf(),
                            new DateRentContractLeaf(),
                        ]),
                ]);

            $leafs[] = $leaf;
        }

        $leafs[] = new ServiceContractEndsLeaf();
        $this->setChildren($leafs);

        return $this;
    }
}
