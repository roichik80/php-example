<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\OwnerTypeDefinition;
use Topnlab\Common\v2\Reference\Secondary\Realty\RealtyActionTypeDefinition;

/**
 * Заявки по спросу (покупке)
 * Class OrderDemandLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class OrderDemandLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Заявки по спросу (покупке)';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (
            $entityType instanceof Task &&
            $entityType->owner_type == OwnerTypeDefinition::AGENCY_CLIENT_ORDER &&
            $entityType->object_action == RealtyActionTypeDefinition::REALTY_ACTION_TYPE_SALE
        ) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', 'like', $this->getHash() . '%');
    }
}
