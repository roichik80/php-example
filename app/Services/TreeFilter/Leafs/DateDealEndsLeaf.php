<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Reminder;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\ReminderSubtypeDefinition;

/**
 * Дата расторжения сделки
 * Class DateDealEndsLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class DateDealEndsLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Дата расторжения сделки';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if ($entityType instanceof Reminder && $entityType->subtype == ReminderSubtypeDefinition::DEAL_ENDS) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', $this->getHash());
        $query->whereBetween('begin_at', [date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')]);
    }

    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'value' => $this->getHash(),
        ];
    }
}
