<?php

namespace App\Services\TreeFilter\Leafs;

use App\Definitions\CustomTaskTypeFlagDefinition;
use App\Models\Db\AbstractEntityType;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTaskTreeLeaf;
use App\Services\TreeFilter\Data\CustomTaksTypeData;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Topnlab\Common\v2\Reference\Calendar\OwnerTypeDefinition;

/**
 * Задачи по лидам
 * Class TaskOnLeadLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class TaskOnLeadLeaf extends AbstractTaskTreeLeaf
{
    protected $title = 'Задачи по лидам';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (($entityType instanceof Task) && $entityType->owner_type == OwnerTypeDefinition::LEAD) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', 'like', $this->getHash() . '%');
    }

    protected function getCustomTaskTypeCollection(CustomTaksTypeData $customTaksTypeData): Collection
    {
        return $customTaksTypeData->getCustomTaskTypeOnlyFlags(CustomTaskTypeFlagDefinition::TEXT_NODE);
    }
}
