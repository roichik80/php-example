<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\OwnerTypeDefinition;

/**
 * Лиды
 * Class LeadLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class LeadLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Лиды';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if ($entityType instanceof Task && $entityType->owner_type == OwnerTypeDefinition::LEAD) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', 'like', $this->getHash() . '%');
    }
}
