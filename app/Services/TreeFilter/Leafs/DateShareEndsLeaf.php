<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Reminder;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\ReminderSubtypeDefinition;

/**
 * Дата окончания акций
 * Class DateShareEndsLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class DateShareEndsLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Дата окончания акций';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (
            $entityType instanceof Reminder &&
            $entityType->subtype == ReminderSubtypeDefinition::SHARE_ENDS
        ) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', $this->getHash());
    }

    public function toArray(): array
    {
        return [
            'title'    => $this->title,
            'value'    => $this->getHash(),
        ];
    }
}
