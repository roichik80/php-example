<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Reminder;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\ReminderSubtypeDefinition;

/**
 * Дата ввода резервных объектов в продажу
 * Class DateEntryReserveRealtyForSaleLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class DateEntryReserveRealtyForSaleLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Дата ввода резервных объектов в продажу';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if ($entityType instanceof Reminder && $entityType->subtype == ReminderSubtypeDefinition::ENTRY_RESERVE_REALTY_FOR_SALE) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', $this->getHash());
        $query->whereBetween('begin_at', [date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')]);
    }

    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'value' => $this->getHash(),
        ];
    }
}
