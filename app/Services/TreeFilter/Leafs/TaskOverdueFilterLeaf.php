<?php

namespace App\Services\TreeFilter\Leafs;

use App\Definitions\EntityTypeDefinition;
use App\Services\TreeFilter\AbstractHiddenTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\TaskStatusDefinition;

class TaskOverdueFilterLeaf extends AbstractHiddenTreeFilterLeaf
{
    protected $title = 'Задача прострочена';

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('entity_type', EntityTypeDefinition::TASK);
        $query->whereHas('task', function($query){
            $query->whereNotNull('end_at');
            $query->where('end_at', '<', date('Y-m-d H:i:s'));
            $query->whereIn('status', [TaskStatusDefinition::DEFAULT, TaskStatusDefinition::NEW]);
        });
    }
}
