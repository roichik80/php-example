<?php

namespace App\Services\TreeFilter\Leafs;

use App\Services\TreeFilter\AbstractRootTreeFilterLeaf;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;

/**
 * Корень дерева собственных (скрытых) фильтров
 * Class RootHiddenTreeFilterLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 */
class RootHiddenTreeFilterLeaf extends AbstractRootTreeFilterLeaf
{
    protected $title = 'hidden';

    public function toArray(): array
    {
        return array_map(function (AbstractTreeFilterLeaf $leaf) {
            return $leaf->toArray();
        }, $this->children);
    }
}
