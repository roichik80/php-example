<?php

namespace App\Services\TreeFilter\Leafs;

use App\Models\Db\AbstractEntityType;
use App\Models\Db\Task;
use App\Services\TreeFilter\AbstractTreeFilterLeaf;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\OwnerTypeDefinition;

/**
 * Диалоги
 * Class DialogLeaf
 *
 * @package App\Services\TreeFilter\Leafs
 * @author Aleksandr Roik
 */
class DialogLeaf extends AbstractTreeFilterLeaf
{
    protected $title = 'Диалоги';

    protected function isEntityFit(AbstractEntityType $entityType): bool
    {
        if (
            $entityType instanceof Task &&
            $entityType->owner_type == OwnerTypeDefinition::DIALOG
        ) {
            return true;
        }

        return false;
    }

    protected function applyMyCondition(Builder $query): void
    {
        $query->where('filter_hash', 'like', $this->getHash() . '%');
    }
}
