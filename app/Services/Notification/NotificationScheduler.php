<?php

namespace App\Services\Notification;

use App\Definitions\NotificationSchedulerDefinition;
use App\Exceptions\PropertyIsNullException;
use App\Extensions\DateTimeHelper;

/**
 * Планировщик уведомлений - анализирует полученняе данные и возвращает дату/время отправки уведомления
 * Class NotificationScheduler
 *
 * @package App\Services\Notification
 * @author Aleksandr Roik
 */
class NotificationScheduler
{
    /**
     * @var Notification
     */
    private $handler;

    /**
     * @var string
     */
    private $executeAt;

    /**
     * @var string|null
     */
    private $remindForPeriod;

    /**
     * @var mixed|null
     */
    private $remindForValue;

    /**
     * NotificationScheduler constructor.
     *
     * @param Notification $handler
     */
    public function __construct(Notification $handler)
    {
        $this->handler = $handler;

        $this->prepare();
    }

    /**
     * Подготавливает параметры
     */
    public function prepare(): void
    {
        $this->prepareRemindFor();
        $this->prepareExecuteAt();
    }

    /**
     * Подготавливает параметры "Напомнить за": remindForPeriod, remindForPeriod
     */
    private function prepareRemindFor(): void
    {
        $schedule = $this->handler->getModel()->schedule;

        if (!$schedule) {
            return;
        }

        if (!empty($schedule['remind_for']['period'])) {
            $this->remindForPeriod = $schedule['remind_for']['period'];
        }

        if (!empty($schedule['remind_for']['value'])) {
            $this->remindForValue = $schedule['remind_for']['value'];
        }

        // проверка
        if (
            in_array($this->remindForPeriod, [
                NotificationSchedulerDefinition::PERIOD_MINUTE,
                NotificationSchedulerDefinition::PERIOD_HOUR,
                NotificationSchedulerDefinition::PERIOD_DAY,
                NotificationSchedulerDefinition::PERIOD_WEEK,
            ])
            && !$this->remindForValue
        ) {
            throw new PropertyIsNullException(sprintf('Значение параметра "%s" равно нулю', "value"));
        }
    }

    /**
     * Подготавливает дату/время отправки сообщения
     */
    private function prepareExecuteAt(): void
    {
        $toSendAt = $this->handler->getModel()->to_send_at;
        $executeAt = null;

        switch ($this->remindForPeriod) {
            case NotificationSchedulerDefinition::PERIOD_MINUTE:
                $executeAt = DateTimeHelper::changeDateTimeByPeriod($toSendAt, "- $this->remindForValue minutes");
                break;
            case NotificationSchedulerDefinition::PERIOD_HOUR:
                $executeAt = DateTimeHelper::changeDateTimeByPeriod($toSendAt, "- $this->remindForValue hours");
                break;
            case NotificationSchedulerDefinition::PERIOD_DAY:
                $executeAt = DateTimeHelper::changeDateTimeByPeriod($toSendAt, "- $this->remindForValue days");
                break;
            case NotificationSchedulerDefinition::PERIOD_WEEK:
                $executeAt = DateTimeHelper::changeDateTimeByPeriod($toSendAt, "- $this->remindForValue weeks");
                break;
            default:
                $executeAt = $toSendAt;
        }

        $this->executeAt = $executeAt;
    }

    /**
     * Проверяет, наступило ли время для отправки.
     * Если $dateTimeForCompare = null - сравнение идет с текущей датой/временем
     *
     * @param string|null $dateTimeForCompare
     * @return bool
     */
    public function timeHasCome(string $dateTimeForCompare = null): bool
    {
        if ($dateTimeForCompare === null) {
            $dateTimeForCompare = date('Y-m-d H:i:s');
        }

        if (
            !$this->executeAt ||
            ($this->executeAt && strtotime($this->executeAt) <= strtotime($dateTimeForCompare))
        ) {
            return true;
        }

        return false;
    }

    /**
     * Возвращает дату/время отправки сообщение
     *
     * @return string
     */
    public function getExecuteAt(): ?string
    {
        return $this->executeAt;
    }
}
