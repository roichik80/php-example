<?php

namespace App\Services\Notification;

use App\Models\Db\Notification as NotificationModel;

/**
 * Сервис обработки уведомлений:
 * - создание расписания
 * - отсылка
 *
 * Class NotificationHandler
 *
 * @package App\Services\Notification
 * @author Aleksandr Roik
 */
class Notification
{
    /**
     * @var NotificationModel
     */
    protected $model;

    /**
     * @var NotificationScheduler
     */
    protected $scheduler;

    /**
     * NotificationScheduler constructor.
     *
     * @param Notification $model
     */
    public function __construct(NotificationModel $model)
    {
        $this->model = $model;
    }

    /**
     * @return NotificationModel
     */
    public function getModel(): NotificationModel
    {
        return $this->model;
    }

    /**
     * @return NotificationScheduler
     */
    private function buildScheduer(): void
    {
        $this->scheduler = (new NotificationScheduler($this));
    }

    /**
     * Возвращает объект расписания NotificationScheduler
     *
     * @return NotificationScheduler
     */
    public function getScheduler(): NotificationScheduler
    {
        if($this->scheduler === null){
            $this->buildScheduer();
        }

        return $this->scheduler;
    }

    /**
     * Отправляет уведомление
     *
     * @param bool $force Принудительная отправка без проверки расписания
     * @return bool
     */
    public function send($force = false)
    {
        return (new NotificationSender($this))
            ->setForce($force)
            ->send();
    }
}
