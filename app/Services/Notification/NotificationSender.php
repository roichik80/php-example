<?php

namespace App\Services\Notification;

use App\Facades\User;
use Topnlab\Common\v2\Connectors\Notification\Connector as NotificationConnector;

/**
 * Class NotificationSender
 * @package App\Services\Notification
 * @author Aleksandr Roik
 */
class NotificationSender
{
    /**
     * @var Notification
     */
    private $handler;

    /**
     * @var NotificationConnector
     */
    private $notificationConnector;

    /**
     * Параметры настройки уведомлений пользователя
     *
     * @var mixed|null
     */
    private $userSettings;

    /**
     * Параметры сообщения
     *
     * @var
     */
    private $messageParams = [];

    /**
     * Принудительная отправка без проверки времени расписания
     *
     * @var bool
     */
    private $force = false;

    /**
     * NotificationScheduler constructor.
     *
     * @param Notification $handler
     */
    public function __construct(Notification $handler)
    {
        $this->handler = $handler;

        $serviceUrl = '';
        $serviceKey = '';
        $this->notificationConnector = new NotificationConnector($serviceUrl, $serviceKey);
    }

    /**
     * @param bool $force
     * @return NotificationSender
     */
    public function setForce(bool $force): self
    {
        $this->force = $force;

        return $this;
    }

    /**
     * @return bool
     */
    public function send(): bool
    {
        if ($this->force == false && !$this->timeHisCome()) {
            return false;
        }

        $this->prepare();

        if (!$this->isEnabledServiceDelivery()) {
            return false;
        }

        return $this->sendPost();
    }

    /**
     * Непосредственно отправка уведомления через коннектор
     *
     * @return bool
     */
    protected function sendPost(): bool
    {
        //TODO: пока заглушка. Надо проянить...
        return true;
    }

    /**
     * Воззвращает статус, наступило ли время для отправки уведдомления
     *
     * @return bool
     */
    protected function timeHisCome()
    {
        return $this->handler->getScheduler()->timeHasCome();
    }

    /**
     * Подготовка параметров
     *
     * @throws \Topnlab\Common\v2\Api\Exception\ApiInvalidResponseException
     * @throws \Topnlab\Common\v2\Api\Exception\ApiRequestCurlException
     */
    public function prepare(): void
    {
        $this->loadUserSittings();
        $this->prepareMessageParams();
    }

    /**
     * Загружает настройки уведомлений пользователя
     *
     * @throws \Topnlab\Common\v2\Api\Exception\ApiInvalidResponseException
     * @throws \Topnlab\Common\v2\Api\Exception\ApiRequestCurlException
     */
    private function loadUserSittings(): void
    {
        return;
        //TODO: Заглушка. Надо активировать конектор
        $userSittings = $this->notificationConnector->getUserSettings(User::getId());

        if ($userSittings) {
            $this->userSettings = $userSittings;
        }
    }

    /**
     * Подготавливает параметры уведомления, что в результате бут отослано
     */
    private function prepareMessageParams(): void
    {
        $messageParams = [];
        //TODO: собрать параметры уведомления
        $this->messageParams = $messageParams;
    }

    /**
     * Проверяет и возвращает статус, включен ли сервис отсылки у пользователя
     */
    private function isEnabledServiceDelivery(): bool
    {
        //TODO: Допылить проверку
        return true;
    }

}
