<?php

namespace App\Extensions;

/**
 * Хелпер для обработки массивов
 * Class ArrayHelper
 *
 * @package App\Extensions
 * @author Aleksandr Roik
 */
class ArrayHelper
{
    /**
     * Слияние  массивов с Добавлением/обновлением элементов второго массива к первому
     * Работает рекурсивно и сливает так же сложенные массивы
     *
     * @param array $array1
     * @param array $array2
     * @return array
     */
    public static function merge(array $array1, array $array2): array
    {
        foreach ($array2 as $key => $value) {
            if (!is_array($value)) {
                $array1[$key] = $value;
            } else {
                if (isset($array1[$key])) {

                    $array1[$key] = self::merge($array1[$key], $value);
                } else {
                    $array1[$key] = $value;
                }
            }
        }

        return $array1;
    }
}
