<?php

namespace App\Extensions;

/**
 * Хелпер для обработки дат
 * Class DateTimeHelper
 *
 * @package App\Extensions
 * @author Aleksandr Roik
 */
final class DateTimeHelper
{
    /**
     * Возвращает диапазон дат (дата начала и конца) исходя из предлоденной даты и типа периода
     *
     * @param string $period
     * @param string $dateTime
     * @return array|null
     */
    public static function getDateTimeRangeByPeriod(string $period, string $dateTime): ?array
    {
        $methodName = 'getDateTimeRangeByPeriod' . ucfirst($period);

        return self::$methodName($dateTime);
    }

    /**
     * Возвращает диапазон дат (дата начала и конца) исходя из предлоденной даты и типа периода "День"
     *
     * @param string $dateTime
     * @return array|null
     */
    public static function getDateTimeRangeByPeriodDay(string $dateTime): ?array
    {
        $beginAt = date('Y-m-d 00:00:00', strtotime($dateTime));
        $endAt = date('Y-m-d 23:59:59', strtotime($dateTime));

        return [
            'beginAt' => $beginAt,
            'endAt'   => $endAt,
        ];
    }

    /**
     * Возвращает диапазон дат (дата начала и конца) исходя из предлоденной даты и типа периода "Месяц"
     *
     * @param string $dateTime
     * @return array|null
     */
    public static function getDateTimeRangeByPeriodMonth(string $dateTime): ?array
    {
        $beginAt = date('Y-m-01 00:00:00', strtotime($dateTime));
        $days = self::getDaysOfMonth($dateTime);
        $endAt = date('Y-m-' . $days . ' 23:59:59', strtotime($dateTime));

        return [
            'beginAt' => $beginAt,
            'endAt'   => $endAt,
        ];
    }

    /**
     * Возвращает диапазон дат (дата начала и конца) исходя из предлоденной даты и типа периода "Неделя"
     *
     * @param string $dateTime
     * @return array|null
     */
    public static function getDateTimeRangeByPeriodWeek(string $dateTime): ?array
    {
        $dateTime = self::getFirstDateTimeWeekInYear($dateTime);
        $beginAt = date('Y-m-d 00:00:00', strtotime($dateTime));

        $dateTime = new \DateTime($dateTime);
        $dateTime->modify('+6 days');
        $endAt = $dateTime->format('Y-m-d 23:59:59');

        return [
            'beginAt' => $beginAt,
            'endAt'   => $endAt,
        ];
    }

    /**
     * Возвращает диапазон дат (дата начала и конца) исходя из предлоденной даты и типа периода "Год"
     *
     * @param string $dateTime
     * @return array|null
     */
    public static function getDateTimeRangeByPeriodYear(string $dateTime): ?array
    {
        $beginAt = date('Y-01-01 00:00:00', strtotime($dateTime));
        $endAt = date('Y-12-31 23:59:59', strtotime($dateTime));

        return [
            'beginAt' => $beginAt,
            'endAt'   => $endAt,
        ];
    }

    /**
     * Возвращает количетсво дней в месяце
     *
     * @param string $dateTime
     * @return string
     */
    public static function getDaysOfMonth(string $dateTime): string
    {
        if (!($dateTime instanceof \DateTime)) {
            $dateTime = new \DateTime($dateTime);
        }
        $dateTmp = new \DateTime($dateTime->format('Y-m-01 H:i:s'));
        $dateTmp->modify('+1 month');
        $dateTmp->modify('-1 day');
        $days = $dateTmp->format('d');

        return $days;
    }

    /**
     * Возвращает дату/время дня недели, исходя из предложенной даты и номера недели
     *
     * @param $dateTime - Дата/время, в неделе которой надо вернуть дату/время требуемого дня
     * @param int $week Номер дня в неделе: 0-воскресенье ... 6-суббота
     * @return null|string
     */
    public static function getFirstDateTimeWeekInYear(string $dateTime, $week = 1): ?string
    {
        if (strtotime($dateTime) === false) {
            return null;
        }
        if (!in_array($week, [0, 1, 2, 3, 4, 5, 6])) {
            return false;
        }

        $dateTime = new \DateTime($dateTime);
        $activeWeek = (integer)$dateTime->format('w');

        if ($week == $activeWeek) {
            return $dateTime->format('Y-m-d H:i:s');
        } else {
            if ($week > $activeWeek) {
                $operator = '+';
                $days = $week - $activeWeek;
            } else {
                $operator = '-';
                $days = $activeWeek - $week;
            }
        }

        $dateTime->modify($operator . $days . ' days');

        return $dateTime->format('Y-m-d H:i:s');
    }

    /**
     * Изменяет представленную дату/время на оперделенный период
     *
     * @param string $dateTime  Строковое представление даты/времени
     * @param string $period    Период, на которое надо изменить
     * @return string
     */
    public static function changeDateTimeByPeriod(string $dateTime, string $period)
    {
        $dateTime = new \DateTime($dateTime);
        $dateTime->modify($period);

        return $dateTime->format('Y-m-d H:i:s');
    }

}
