<?php

namespace App\Extensions;

/**
 * Хелпер для обработки данных или конструкутора запросов для базы данных
 * Class QueryHelper
 *
 * @package App\Extensions
 * @author Aleksandr Roik
 */
class QueryHelper
{

    /**
     * Заменяет каждый символ строки с $from по $to на $replacement
     *
     * @param $value - входная строка
     * @param int $from
     * @param int|null $to
     * @param string $replacement - строка для замены
     * @return mixed
     */
    public static function replaceValueToLike($value, $from = 0, $to = null, $replacement = '_')
    {
        if ($to === null) {
            $len = mb_strlen($value);
            $to = $len - $from;
        }

        if ($replacement == '_') {
            $replacement = '';
            for ($i = 1; $i <= $to; $i++) {
                $replacement .= '_';
            }
        }

        return substr_replace($value, $replacement, $from, $to);
    }

}
