<?php

namespace App\Exceptions;

use Throwable;

/**
 * Исключение, если значение параметра отсутвует
 * Class PropertyNotFoundException
 *
 * @package App\Exceptions
 * @author Aleksandr Roik
 */
class PropertyNotFoundException extends AbstractRuntimeException
{
    /**
     * PropertyIsNullException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = 'Отсутствует параметр', int $code = 423, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
