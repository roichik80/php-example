<?php

namespace App\Exceptions;

use Throwable;

/**
 * Исключение сохранения или обновление записи в БД
 * Class ModelNotSaveException
 *
 * @package App\Exceptions
 * @author Alexey Yermakov slims.alex@gmail.com
 */
class ModelNotSaveException extends AbstractRuntimeException
{
    /**
     * ModelNotSaveException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = 'Не удалось сохранить запис', int $code = 423, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
