<?php

namespace App\Exceptions;

use Throwable;

/**
 * Исключение при удалении записи их БД
 * Class ModelNotDeleteException
 *
 * @package App\Exceptions
 * @author Aleksandr Roik
 */
class ModelNotDeleteException extends AbstractRuntimeException
{
    /**
     * ModelNotDeleteException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = 'Не удалось удалить запись', int $code = 423, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
