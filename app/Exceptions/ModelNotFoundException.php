<?php

namespace App\Exceptions;

use Throwable;

/**
 * Исключение поиска записи в БД
 * Class ModelNotFoundException
 *
 * @package App\Exceptions
 * @author Alexey Yermakov slims.alex@gmail.com
 */
class ModelNotFoundException extends AbstractRuntimeException
{
    /**
     * ModelNotFoundException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = 'Не удалось найти запись', int $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

