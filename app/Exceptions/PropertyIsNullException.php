<?php

namespace App\Exceptions;

use Throwable;

/**
 * Исключение, если значение параметра равно NULL
 * Class PropertyIsNullException
 *
 * @package App\Exceptions
 * @author Aleksandr Roik
 */
class PropertyIsNullException extends AbstractRuntimeException
{
    /**
     * PropertyIsNullException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = 'Значение параметра равно нулю', int $code = 423, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
