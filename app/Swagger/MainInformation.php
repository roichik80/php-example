<?php
namespace App\Swagger;

use OpenApi\Annotations as OA;

/**
 * Общие блоки, модели для документации
 * Class MainInformation
 *
 * @package App\Swagger
 * @author Aleksandr Roik
 */
class MainInformation
{
    /**
     * ---------------------------------
     * Основная информация для построения Swagger
     * ---------------------------------
     * @OA\Info(
     *      version="1.0",
     *      title="Calendar Api",
     *      description="Микро сервис календаря",
     *      @OA\Contact(
     *             name="Aleksanr Roik, Alexey Yermakov",
     *             email="roichik.ov@gmail.com, slims.alex@gmail.com"
     *      ),
     * )
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Calendar Api"
     * )
     * @OA\SecurityScheme(
     *     securityScheme="auth",
     *     type="apiKey",
     *     name="auth-key",
     *     in="header",
     *     description="A value of the authorization header"
     * )
     * ---------------------------------
     * Общие поля (списки полей)
     * ---------------------------------
     * @OA\Schema(
     *     schema="CompanyField",
     *     @OA\Property(property="company_id", type="integer", description="ID  компании", example=""),
     *)
     * @OA\Schema(
     *     schema="UserField",
     *     @OA\Property(property="user_id", type="integer", description="ID пользователя, что создал сущность", example=""),
     *)
     * @OA\Schema(
     *     schema="TechnicalFields",
     *     @OA\Property(property="id", type="integer", description="ID записи"),
     *     @OA\Property(property="created_at", type="string", description="Дата создания сущности", example=""),
     *     @OA\Property(property="updated_at", type="string", description="Дата последнего изменения сущности", example=""),
     *)
     * ---------------------------------
     * Возвращение результата. Исключения
     * ---------------------------------
     * @OA\Schema(
     *     schema="ModelNotFoundResponse",
     *     @OA\Property(property="status", type="string", description="Статус", example="error"),
     *     @OA\Property(property="error", type="string", description="Ошибка поиска записи по id", example="Не удалось найти запись с id: <ID>")
     * )
     * @OA\Schema(
     *     schema="ModelNotSaveResponse",
     *     @OA\Property(property="status", type="string", description="Статус", example="error"),
     *     @OA\Property(property="error", type="string", description="Ошибка сохранения записи", example="Не удалось сохранить запись")
     * )
     * @OA\Schema(
     *     schema="ModelNotDeleteResponse",
     *     @OA\Property(property="status", type="string", description="Статус", example="error"),
     *     @OA\Property(property="error", type="string", description="Ошибка удаления записи", example="Не удалось удалить запись")
     * )
     * @OA\Schema(
     *     schema="PropertyIsNullResponse",
     *     @OA\Property(property="status", type="string", description="Статус", example="error"),
     *     @OA\Property(property="error", type="string", description="Ошибка проверки параметров", example="Значение параметра равно нулю")
     * )
     * @OA\Schema(
     *     schema="PropertyNotFoundException",
     *     @OA\Property(property="status", type="string", description="Статус", example="error"),
     *     @OA\Property(property="error", type="string", description="Отсутствует параметр", example="Отсутствует параметр")
     * )
     * ---------------------------------
     * Возвращение результата
     * ---------------------------------
     * @OA\Schema(
     *     schema="SuccessResponse",
     *     @OA\Property(property="status", type="string", description="Статус", example="success"),
     * )
     * @OA\Schema(
     *     schema="SuccessDataTrueResponse",
     *     allOf={
     *          @OA\Schema(ref="#/components/schemas/SuccessResponse"),
     *          @OA\Schema(
     *              @OA\Property(property="data", type="boolean", example="true")
     *          )
     *     }
     * )
     * @OA\Schema(
     *     schema="ValidationErrorResponse",
     *     @OA\Property(property="status", type="string", description="Статус", example="error"),
     *          @OA\Property(property="error", type="object", description="Список полей с массивом ошибок",
     *              allOf={
     *                  @OA\Schema(
     *                      @OA\Property(property="begin_at", type="array",
     *                          @OA\Items()
     *                      )
     *                  )
     *              }
     *          )
     *      )
     * )
     */
}
