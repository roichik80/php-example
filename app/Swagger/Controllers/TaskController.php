<?php
namespace App\Swagger\Controllers;

use OpenApi\Annotations as OA;

/**
 * Документация. Задачи
 * Class TaskController
 *
 * @package App\Swagger\Controllers
 * @author Aleksandr Roik
 */
class TaskController
{
    /**
     * Создание задачи
     * @OA\Post(
     *     path="/task",
     *     tags={"Task"},
     *     description="Создание задачи",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TaskFillableModel")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Положительный ответ",
     *         @OA\JsonContent(ref="#/components/schemas/SuccessDataTrueResponse")
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Ошибка валидации",
     *         @OA\JsonContent(ref="#/components/schemas/ValidationErrorResponse")
     *     ),
     *     @OA\Response(
     *         response="423",
     *         description="Ошибка сохранения записи",
     *         @OA\JsonContent(ref="#/components/schemas/ModelNotSaveResponse")
     *     ),
     *     security={{
     *        "auth":{}
     *     }}
     *  )
     */

    /**
     * Получение задачи по идентификатору
     * @OA\Get(
     *     path="/task/{id}",
     *     tags={"Task"},
     *     description="Получение задачи по идентификатору",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="Идентификатор сущности",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Положительный ответ",
     *         @OA\JsonContent(ref="#/components/schemas/TaskSuccessDataResponse")
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Запись не найдена",
     *         @OA\JsonContent(ref="#/components/schemas/ModelNotFoundResponse")
     *     ),
     *     security={{
     *        "auth":{}
     *     }}
     *  )
     */

    /**
     * Редактирование задачи
     * @OA\Put(
     *     path="/task/{id}",
     *     tags={"Task"},
     *     description="Редактирование задачи",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="Идентификатор сущности",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/TaskFillableModel")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Положительный ответ",
     *         @OA\JsonContent(ref="#/components/schemas/SuccessDataTrueResponse")
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Запись не найдена",
     *         @OA\JsonContent(ref="#/components/schemas/ModelNotFoundResponse")
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Ошибка валидации",
     *         @OA\JsonContent(ref="#/components/schemas/ValidationErrorResponse")
     *     ),
     *     @OA\Response(
     *         response="423",
     *         description="Ошибка сохранения записи",
     *         @OA\JsonContent(ref="#/components/schemas/ModelNotSaveResponse")
     *     ),
     *     security={{
     *        "auth":{}
     *     }}
     *  )
     */

    /**
     * Удаление задачи
     * @OA\Delete(
     *     path="/task/{id}",
     *     tags={"Task"},
     *     description="Удаление задачи",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="Идентификатор сущности",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Положительный ответ",
     *         @OA\JsonContent(ref="#/components/schemas/SuccessDataTrueResponse")
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Запись не найдена",
     *         @OA\JsonContent(ref="#/components/schemas/ModelNotFoundResponse")
     *     ),
     *     @OA\Response(
     *         response="423",
     *         description="Ошибка удаления записи",
     *         @OA\JsonContent(ref="#/components/schemas/ModelNotDeleteResponse")
     *     ),
     *     security={{
     *        "auth":{}
     *     }}
     *  )
     */
}
