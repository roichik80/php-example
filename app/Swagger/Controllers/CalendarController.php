<?php
namespace App\Swagger\Controllers;

use OpenApi\Annotations as OA;

/**
 * Документация для действий Календаря
 * Class CalendarController
 *
 * @package App\Documentation\Controllers
 */
class CalendarController
{
    /**
     * @OA\Get(
     *      path="/calendar/data-by-id/{calendar_id}",
     *      operationId="dataById",
     *      tags={"Calendar"},
     *      summary="Получение сущности по идентификатору",
     *      description="Получение сущности по идентификатору",
     *      @OA\Parameter(
     *          name="calendar_id",
     *          description="ID сущности календаря",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Положительный ответ.<br>Свойство 'data' может содержать только один из предложеннного списка полей, что соответсвует определенному типу сущности: Задача, Мероприйтие, Напоминание, Заметка о событии и Текстовая заметка.<br>Список полей смотрите в описании определенной модели - закладка Model, data->oneOf[]",
     *         @OA\JsonContent(ref="#/components/schemas/CalendarDataById")
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Запись не найдена",
     *         @OA\JsonContent(ref="#/components/schemas/ModelNotFoundResponse")
     *     ),
     * )
     */

    /**
     * @OA\Post(
     *      path="/calendar/data-by-date-time-range",
     *      operationId="dataByDateTimeRange",
     *      tags={"Calendar"},
     *      summary="Получить список сущностей календаря за предложеный период времени",
     *      description="Получить список сущностей календаря за предложеный период времени",
     *      @OA\Parameter(
     *          name="begin_at",
     *          description="Дата/время начала периода",
     *          required=true,
     *          example="2019-01-01 00:00:00",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="end_at",
     *          description="Дата/время конца периода",
     *          required=true,
     *          example="2019-12-31 23:59:59",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="Литированное количество записей",
     *          required=true,
     *          example="50",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              minLength=1,
     *              maxLength=50,
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="filters[]",
     *          description="Список фильтров",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="states[]",
     *          description="Список состояний",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="offset",
     *          description="Номер записи, начиная с которой нужно вернуть данные",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              minLength=1,
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="use_time_slice",
     *          description="Задает группировку данных сущностей по часовой метке с интервалом в 15 мин. Значения: 0 (по умолчанию), 1",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              minLength=0,
     *              maxLength=1,
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="entity_types[]",
     *          description="Список типов сущностей, данные по которым надо возвратить",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="fields[]",
     *          description="Список полей, что надо возвратить для сущности. Даные передаются в многомерном массиме, где: ключ - код типа сущности (entityType), значение - массив полей или значение true (возвращает список всех полей)<br>",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="order[]",
     *          description="Список полей для сортировки. После названия поля можно указать направление сортировки (asc|desc). Пример: begin_at desc. По умолчанию сортировка по полю begin_at. Доступны только публичные поля, т.е. те поля, что возвращаются в ответе и существуют во всех типах сущностей (begin_at, end_at)",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Положительный ответ.<br>Свойство 'data' содержит массив всех сущностей с набором полей, что соответсвуют определенному ее типу: Задача, Мероприйтие, Напоминание, Заметка о событии и Текстовая заметка.<br>Список дополнительных полей смотрите в описании моделей - закладка Model, data->anyOf[]",
     *         @OA\JsonContent(ref="#/components/schemas/CalendarDataByDateTimeRange")
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Ошибка валидации",
     *         @OA\JsonContent(ref="#/components/schemas/ValidationErrorResponse")
     *     ),
     *      security={
     *     },
     * )
     */

    /**
     * @OA\Post(
     *      path="/calendar/data-by-period",
     *      operationId="dataByPeriod",
     *      tags={"Calendar"},
     *      summary="Возвращает список сущностей календаря за указанный период",
     *      description="Возвращает список сущностей календаря за определенный период.<br>В процессе отработки на основании парамеров запросса period и date_time будет расчитан диапазон дат, за который будут выгружены данные.<br>К примеру: если передан период month - будет взят период за месяц, изходя из значения даты date_time которому она соответствует, для year - период за определенный год",
     *      @OA\Parameter(
     *          name="period",
     *          description="Значение периода, за который нужно возвратить данные",
     *          required=true,
     *          example="year",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="date_time",
     *          description="Дата/время для вычисления периода",
     *          required=true,
     *          example="2019-01-01 00:00:00",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="Литированное количество записей",
     *          required=true,
     *          example="50",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              minLength=1,
     *              maxLength=50,
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="filters[]",
     *          description="Список фильтров",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="states[]",
     *          description="Список состояний",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="offset",
     *          description="Номер записи, начиная с которой нужно вернуть данные",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              minLength=1,
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="use_time_slice",
     *          description="Задает группировку данных сущностей по часовой метке с интервалом в 15 мин. Значения: 0 (по умолчанию), 1",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              minLength=0,
     *              maxLength=1,
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="entity_types[]",
     *          description="Список типов сущностей, данные по которым надо возвратить",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="fields[]",
     *          description="Список полей, что надо возвратить для сущности. Даные передаются в многомерном массиме, где: ключ - код типа сущности (entityType), значение - массив полей или значение true (возвращает список всех полей)<br>",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="order[]",
     *          description="Список полей для сортировки. После названия поля можно указать направление сортировки (asc|desc). Пример: begin_at desc. По умолчанию сортировка по полю begin_at. Доступны только публичные поля, т.е. те поля, что возвращаются в ответе и существуют во всех типах сущностей (begin_at, end_at)",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Положительный ответ.<br>Свойство 'data' содержит массив всех сущностей с набором полей, что соответсвуют определенному ее типу: Задача, Мероприйтие, Напоминание, Заметка о событии и Текстовая заметка.<br>Список дополнительных полей смотрите в описании моделей - закладка Model, data->anyOf[]",
     *         @OA\JsonContent(ref="#/components/schemas/CalendarDataByDateTimeRange")
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Ошибка валидации",
     *         @OA\JsonContent(ref="#/components/schemas/ValidationErrorResponse")
     *     ),
     *      security={
     *     },
     * )
     */

    /**
     * @OA\Post(
     *      path="/calendar/data-count-by-date-time-range",
     *      operationId="dataCountByDateTimeRange",
     *      tags={"Calendar"},
     *      summary="Возвращает список количества сущностей в календаре за определенный период",
     *      description="Возвращает список количества сущностей в календаре за определенный период",
     *      @OA\Parameter(
     *          name="begin_at",
     *          description="Дата/время начала периода",
     *          required=true,
     *          example="2019-01-01 00:00:00",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="end_at",
     *          description="Дата/время конца периода",
     *          required=true,
     *          example="2019-12-31 23:59:59",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="Литированное количество записей",
     *          required=true,
     *          example="50",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              minLength=1,
     *              maxLength=50,
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="filters[]",
     *          description="Список фильтров",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="states[]",
     *          description="Список состояний",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="offset",
     *          description="Номер записи, начиная с которой нужно вернуть данные",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              minLength=1,
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="use_time_slice",
     *          description="Задает группировку данных сущностей по часовой метке с интервалом в 15 мин. Значения: 0 (по умолчанию), 1",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              minLength=0,
     *              maxLength=1,
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="entity_types[]",
     *          description="Список типов сущностей, данные по которым надо возвратить",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="fields[]",
     *          description="Список полей, что надо возвратить для сущности. Даные передаются в многомерном массиме, где: ключ - код типа сущности (entityType), значение - массив полей или значение true (возвращает список всех полей)<br>",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="order[]",
     *          description="Список полей для сортировки. После названия поля можно указать направление сортировки (asc|desc). Пример: begin_at desc. По умолчанию сортировка по полю begin_at. Доступны только публичные поля, т.е. те поля, что возвращаются в ответе и существуют во всех типах сущностей (begin_at, end_at)",
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(),
     *          )
     *      ),
     *      @OA\Response(
     *         response="200",
     *         description="Положительный ответ.<br>Свойство 'data' содержит объект со список данных о количестве сущностей за определенный период времени,<br> где: ключ - ДатаВремя, значение - количество сущностей",
     *         @OA\JsonContent(ref="#/components/schemas/CalendarDataCountByDateTimeRange")
     *      ),
     *      @OA\Response(
     *         response="422",
     *         description="Ошибка валидации",
     *         @OA\JsonContent(ref="#/components/schemas/ValidationErrorResponse")
     *      ),
     *         security={
     *      },
     * )
     */

    /**
     * @OA\Get(
     *      path="/calendar/filter-list",
     *      operationId="filterList",
     *      tags={"Calendar"},
     *      summary="Возвращает список фильтров",
     *      description="Возвращает список фильтров.<br>Свойство 'data' содержит асоциативный массив с данными фильтров",
     *      @OA\Response(
     *          response=200,
     *          description="Положительный ответ",
     *          @OA\JsonContent(ref="#/components/schemas/CalendarFilterList")
     *      ),
     *          security={
     *      },
     * )
     */

    /**
     * @OA\Get(
     *      path="/calendar/period-list",
     *      operationId="periodList",
     *      tags={"Calendar"},
     *      summary="Возвращает список периодов",
     *      description="Возвращает список периодов",
     *      @OA\Response(
     *          response=200,
     *          description="Положительный ответ",
     *          @OA\JsonContent(ref="#/components/schemas/CalendarPeriodList")
     *      ),
     *          security={
     *      },
     * )
     */

    /**
     * @OA\Get(
     *      path="/calendar/state-list",
     *      operationId="stateList",
     *      tags={"Calendar"},
     *      summary="Возвращает список состояний",
     *      description="Возвращает список состояний",
     *      @OA\Response(
     *          response=200,
     *          description="Положительный ответ",
     *          @OA\JsonContent(ref="#/components/schemas/CalendarStateList")
     *      ),
     *          security={
     *      },
     * )
     */

    /**
     * @OA\Get(
     *      path="/calendar/entity-type-list",
     *      operationId="entityTypeList",
     *      tags={"Calendar"},
     *      summary="Возвращает список типов сущностей",
     *      description="Возвращает список типов сущностей",
     *      @OA\Response(
     *          response=200,
     *          description="Положительный ответ",
     *          @OA\JsonContent(ref="#/components/schemas/CalendarEntityTypeList")
     *      ),
     *          security={
     *      },
     * )
     */
}
