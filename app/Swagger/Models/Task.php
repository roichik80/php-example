<?php
namespace App\Swagger\Models;

use OpenApi\Annotations as OA;

/**
 * Модель. Задачи
 * Class Task
 *
 * @package App\Swagger\Models
 * @author Aleksandr Roik
 */
class Task
{
    /**
     * @OA\Schema(
     *      schema="TaskFillableModel",
     *      @OA\Property(property="begin_at", type="string", description="Дата начала выполненния задачи", example="2019-01-08 12:25:00"),
     *      @OA\Property(property="end_at", type="string", description="Дата завершения выполненния задачи", example=""),
     *      @OA\Property(property="subtype", type="integer", description="Субтип. Опеределяет дополнительную логику", example="100000000"),
     *      @OA\Property(property="owner_id", type="integer", description="ID сущности внешнего ресурса", example="1"),
     *      @OA\Property(property="owner_type", type="integer", description="Тип внешнего ресурса", example="100000"),
     *      @OA\Property(property="complex_id", type="integer", description="ЖК", example="1"),
     *      @OA\Property(property="description", type="string", description="Описание задачи", example="Description 1"),
     *      @OA\Property(property="custom_task_type_id", type="integer", description="ID типа задачи", example="1"),
     *      @OA\Property(property="object_status", type="integer", description="Статус объекта", example="1"),
     *      @OA\Property(property="object_action", type="integer", description="Тип сделки", example="rent"),
     *      @OA\Property(property="parent_id", type="integer", description="ID родительской сущности из которой клонирована текущая сущность", example="1"),
     *      @OA\Property(property="status", type="integer", description="Статус выполнения задачи", example="1"),
     *      @OA\Property(property="comment", type="string", description="Комментарии к выполнению", example="Comment 1"),
     *      @OA\Property(property="expiration_at", type="string", description="Дата завершения задачи", example=""),
     *      @OA\Property(property="participants", type="array", description="Список участников",
     *          @OA\Items(
     *          )
     *      ),
     *      @OA\Property(property="notifications", type="array", description="Список уведомлений",
     *          @OA\Items(
     *          )
     *      )
     * )
     * @OA\Schema(
     *      schema="TaskDataByCalendarModel",
     *      @OA\Property(property="calendar_id", type="integer", description="ID календаря", example="1"),
     *      @OA\Property(property="entity_id", type="integer", description="ID сущности", example="1"),
     *      @OA\Property(property="entity_type", type="integer", description="Тип сущности", example="1"),
     *      @OA\Property(property="begin_at", type="string", description="Дата начала выполненния задачи", example="2019-01-08 12:25:00"),
     *      @OA\Property(property="end_at", type="string", description="Дата завершения выполненния задачи", example=""),
     *      @OA\Property(property="user_id", type="string", description="ID пользователя, что создал сущность", example="1"),
     *      @OA\Property(property="subtype", type="integer", description="Субтип. Опеределяет дополнительную логику", example="100000000"),
     *      @OA\Property(property="owner_id", type="integer", description="ID сущности внешнего ресурса", example="1"),
     *      @OA\Property(property="owner_type", type="integer", description="Тип внешнего ресурса", example="100000"),
     *      @OA\Property(property="custom_task_type_id", type="integer", description="ID типа задачи", example="1"),
     *      @OA\Property(property="parent_id", type="integer", description="ID родительской сущности из которой клонирована текущая сущность", example="1"),
     *      @OA\Property(property="status", type="integer", description="Статус выполнения задачи", example="1"),
     *      @OA\Property(property="title", type="string", description="Название задачи", example="Title 1"),
     * )
     * @OA\Schema(
     *      schema="TaskDataModel",
     *      @OA\Property(property="calendar_id", type="integer", description="ID календаря", example="1"),
     *      @OA\Property(property="entity_id", type="integer", description="ID сущности", example="1"),
     *      @OA\Property(property="entity_type", type="integer", description="Тип сущности", example="1"),
     *      @OA\Property(property="begin_at", type="string", description="Дата начала выполненния задачи", example="2019-01-08 12:25:00"),
     *      @OA\Property(property="end_at", type="string", description="Дата завершения выполненния задачи", example="2019-01-09 12:25:00"),
     *      @OA\Property(property="begin_at_moved", type="string", description="Дата начала выполненния из перенесенной сущности", example="2019-01-08 12:25:00"),
     *      @OA\Property(property="end_at_moved", type="string", description="Дата завершения выполненния из перенесенной сущности", example="2019-01-09 12:25:00"),
     *      @OA\Property(property="user_id", type="string", description="ID пользователя, что создал сущность", example="1"),
     *      @OA\Property(property="subtype", type="integer", description="Субтип. Опеределяет дополнительную логику", example="100000000"),
     *      @OA\Property(property="owner_id", type="integer", description="ID сущности внешнего ресурса", example="1"),
     *      @OA\Property(property="owner_type", type="integer", description="Тип внешнего ресурса", example="100000"),
     *      @OA\Property(property="complex_id", type="integer", description="ЖК", example="1"),
     *      @OA\Property(property="custom_task_type_id", type="integer", description="ID типа задачи", example="1"),
     *      @OA\Property(property="object_status", type="integer", description="Статус объекта", example="1"),
     *      @OA\Property(property="object_action", type="integer", description="Тип сделки", example="rent"),
     *      @OA\Property(property="parent_id", type="integer", description="ID родительской сущности из которой клонирована текущая сущность", example="1"),
     *      @OA\Property(property="children_id", type="integer", description="ID перенесенной сущности", example="1"),
     *      @OA\Property(property="status", type="integer", description="Статус выполнения задачи", example="1"),
     *      @OA\Property(property="title", type="string", description="Название задачи", example="Title 1"),
     *      @OA\Property(property="description", type="string", description="Описание задачи", example="Description 1"),
     *      @OA\Property(property="comment", type="string", description="Комментарии к выполнению", example="Comment 1"),
     *      @OA\Property(property="expiration_at", type="string", description="Дата завершения задачи", example="2019-01-08 12:25:00"),
     *      @OA\Property(property="participants", type="array", description="Список участников",
     *          @OA\Items(
     *          )
     *      ),
     *      @OA\Property(property="notifications", type="array", description="Список уведомлений",
     *          @OA\Items(
     *          )
     *      )
     * )
     * ---------------------------------
     * Возвращение результата
     * ---------------------------------
     * @OA\Schema(
     *     schema="TaskSuccessDataResponse",
     *     allOf={
     *          @OA\Schema(ref="#/components/schemas/SuccessResponse"),
     *          @OA\Schema(
     *              @OA\Property(property="data", type="object",
     *                  allOf={
     *                      @OA\Schema(ref="#/components/schemas/TaskDataModel"),
     *                  }
     *              )
     *          )
     *     }
     * )
     */
}
