<?php

namespace App\Swagger\Models;

use OpenApi\Annotations as OA;

/**
 * Class Calendar
 *
 * @package App\Swagger\Models
 * @author Alexey Yermakov slims.alex@gmail.com
 */
class Calendar
{
    /**
     * @OA\Schema(
     *     schema="CalendarDataById",
     *     description="Список полей сущности Календаря",
     *     allOf={
     *          @OA\Schema(ref="#/components/schemas/SuccessResponse"),
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      oneOf={
     *                          @OA\Schema(ref="#/components/schemas/TaskDataByCalendarModel"),
     *                          @OA\Schema(ref="#/components/schemas/MeetingDataByCalendarModel"),
     *                          @OA\Schema(ref="#/components/schemas/ReminderDataByCalendarModel"),
     *                          @OA\Schema(ref="#/components/schemas/EventNoteDataByCalendarModel"),
     *                          @OA\Schema(ref="#/components/schemas/TextNoteDataByCalendarModel"),
     *                      }
     *                  )
     *              )
     *          )
     *     }
     * )
     */

    /**
     * @OA\Schema(
     *     schema="CalendarDataByDateTimeRange",
     *     description="Список сущностей Календаря за период",
     *     allOf={
     *          @OA\Schema(ref="#/components/schemas/SuccessResponse"),
     *          @OA\Items(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      anyOf={
     *                          @OA\Schema(ref="#/components/schemas/TaskDataByCalendarModel"),
     *                          @OA\Schema(ref="#/components/schemas/MeetingDataByCalendarModel"),
     *                          @OA\Schema(ref="#/components/schemas/ReminderDataByCalendarModel"),
     *                          @OA\Schema(ref="#/components/schemas/EventNoteDataByCalendarModel"),
     *                          @OA\Schema(ref="#/components/schemas/TextNoteDataByCalendarModel"),
     *                      }
     *                  )
     *              )
     *          )
     *     }
     * )
     */

    /**
     * @OA\Schema(
     *     schema="CalendarDataCountByDateTimeRange",
     *     description="Список количества сущностей Календаря за период",
     *     allOf={
     *          @OA\Schema(ref="#/components/schemas/SuccessResponse"),
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="data",
     *                  type="object",
     *                  @OA\Property(
     *                     property="2019-01-01",
     *                     type="integer",
     *                     example=10,
     *                      )
     *              )
     *          )
     *     }
     * )
     */

    /**
     * @OA\Schema(
     *     schema="CalendarFilterList",
     *     description="Список фильтров",
     *     allOf={
     *          @OA\Schema(ref="#/components/schemas/SuccessResponse"),
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="data",
     *                  type="object",
     *                  description="Содержит асоциативный массив (объект) с данными фильтров",
     *              )
     *          )
     *     }
     * )
     */

    /**
     * @OA\Schema(
     *     schema="CalendarPeriodList",
     *     description="Список периодов",
     *     allOf={
     *          @OA\Schema(ref="#/components/schemas/SuccessResponse"),
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      @OA\Property(
     *                          property="value",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *     }
     * )
     */

    /**
     * @OA\Schema(
     *     schema="CalendarStateList",
     *     description="Список состояний",
     *     allOf={
     *          @OA\Schema(ref="#/components/schemas/SuccessResponse"),
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      @OA\Property(
     *                          property="value",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *     }
     * )
     */

    /**
     * @OA\Schema(
     *     schema="CalendarEntityTypeList",
     *     description="Список типов сущностей",
     *     allOf={
     *          @OA\Schema(ref="#/components/schemas/SuccessResponse"),
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      @OA\Property(
     *                          property="value",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *     }
     * )
     */
}
