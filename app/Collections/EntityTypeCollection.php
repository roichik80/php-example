<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

/**
 * Class EntityTypeCollection
 * Коллекция для моделей типов сущностей
 *
 * @package App\Collections
 * @author Aleksandr Roik
 */
class EntityTypeCollection extends Collection
{
    /**
     * Возвращает список сущностей для календаря
     *
     * @return array
     */
    public function getAllByCalendarAsArray(): array
    {
        $result = [];
        foreach ($this->items as $model) {
            $result[] = $model->getDataByCalendar();
        }

        return $result;
    }

    /**
     * Возвращает список сущностей
     *
     * @return array
     */
    public function getAllAsArray(): array
    {
        $result = [];
        foreach ($this->items as $model) {
            $result[] = $model->getData();
        }

        return $result;
    }

}
