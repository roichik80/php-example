<?php

namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

/**
 * Class CalendarEntityCollection
 * Коллекция для модели Calendar
 *
 * @package App\Collections
 * @author Aleksandr Roik
 */
class CalendarEntityCollection extends Collection
{
    /**
     * Список полей, что надо возвратить для сущности
     *
     * @var array
     */
    protected $responseFields;

    /**
     * Установка списка полей, что надо возвратить для сущности
     *
     * @param $responseFields
     * @return $this
     */
    public function setResponseFields($responseFields): self
    {
        $this->responseFields = $responseFields;

        return $this;
    }

    /**
     * Возвращает список сущностей календаря
     *
     * @return array
     */
    public function getAllAsArray(): array
    {
        $result = [];

        foreach ($this->items as $model) {
            $result[] = $model
                ->setResponseFields($this->responseFields)
                ->getData();
        }

        return $result;
    }

    /**
     * Возвращает количество сущностей календаря, что были сгрупированны
     * ранее по некому признаку
     *
     * @return array
     */
    public function getCountsByDateRangeAsArray(): array
    {
        $result = [];
        foreach ($this->items as $model) {
            $result[$model['session_id']] = $model['count_rows'];
        }

        return $result;
    }
}
