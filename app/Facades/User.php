<?php
namespace App\Facades;

use App\Services\User\Contracts\UserInterface;
use Illuminate\Support\Facades\Facade;

/**
 * Фасад для работы с Сервисом пользователей
 * Class User
 *
 * @package App\Facades
 */
class User extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return UserInterface::class;
    }
}
