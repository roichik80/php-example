<?php
namespace App\Facades;

use App\Services\Connector\Contracts\ConnectorInterface;
use Illuminate\Support\Facades\Facade;

/**
 * Class ConnectorAuth
 *
 * @package App\Facades
 */
class ConnectorAuth extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return ConnectorInterface::class;
    }
}
