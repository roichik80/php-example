<?php

namespace App\Definitions;

/**
 * Справочник периодов
 * Class PeriodDefinition
 *
 * @package App\Definitions
 * @author Aleksandr Roik
 */
class PeriodDefinition extends AbstractDefinition
{
    /**
     * Временные периоды
     *
     * @const string
     */
    const DAY = 'day';
    const WEEK = 'week';
    const MONTH = 'month';
    const YEAR = 'year';
    const LIST = 'list';

    /**
     * Список всех периодов
     *
     * @var array
     */
    private static $periodCollection = [
        self::DAY,
        self::WEEK,
        self::MONTH,
        self::YEAR,
        self::LIST,
    ];

    private static $periodTitleCollection = [
        self::DAY => 'День',
        self::WEEK => 'Неделя',
        self::MONTH => 'Месяц',
        self::YEAR => 'Год',
        self::LIST => 'Список задач',
    ];

    /**
     * Возвращает список периодов
     *
     * @return array
     */
    public static function getPeriodCollection(): array
    {
        return self::$periodCollection;
    }

    /**
     * Возвращает список названий периодов
     *
     * @return array
     */
    public static function getPeriodTitleCollection(): array
    {
        return self::$periodTitleCollection;
    }

    /**
     * Проверяет наличие периода
     *
     * @param $period string
     * @return bool
     */
    public static function isValidPeriod(string $period): bool
    {
        return in_array($period, self::$periodCollection);
    }
}
