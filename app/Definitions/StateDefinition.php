<?php

namespace App\Definitions;

/**
 * Справочник состояний
 * Class StatusDefinition
 *
 * @package App\Definitions
 * @author Aleksandr Roik
 */
class StateDefinition extends AbstractDefinition
{
    /**
     * Состояния
     *
     * @const int
     */
    const OVERDUE = 1; // Прострочена
    const MOVED = 2; // Перенесена
    const OPENED = 3; // Открыта
    const COMPLETED = 4; // Выполнена
    const CANCELED = 5; // Отменена

    /**
     * Список состояний
     *
     * @var array
     */
    private static $typeCollection = [
        self::OVERDUE,
        self::MOVED,
        self::OPENED,
        self::COMPLETED,
        self::CANCELED,
    ];

    /**
     * Список публичных состояний
     *
     * @var array
     */
    private static $publicTypeCollection = [
        self::OPENED,
        self::COMPLETED,
        self::CANCELED,
    ];

    /**
     * Список названия состояний
     *
     * @var array
     */
    private static $typeTitleCollection = [
        self::OVERDUE   => 'Задача прострочена',
        self::MOVED     => 'Задача перенесена',
        self::OPENED    => 'Задача открыта',
        self::COMPLETED => 'Задача выполнена',
        self::CANCELED  => 'Задача отменена',
    ];

    /**
     * Возвращает список названий типов
     *
     * @return array
     */
    public static function getTypeTitleCollection(): array
    {
        return static::$typeTitleCollection;
    }

    /**
     * Возвращает список публичных типов
     *
     * @return array
     */
    public static function getPublicTypeCollection(): array
    {
        return self::$publicTypeCollection;
    }

    /**
     * Возвращает список публичных названий типов
     *
     * @return array
     */
    public static function getPublicTypeTitleCollection(): array
    {
        return array_filter(self::$typeTitleCollection, function($v, $k){
            return in_array($k, self::$publicTypeCollection);
        }, ARRAY_FILTER_USE_BOTH);
    }

    /**
     * Возвращает название по типу
     *
     * @return array
     */
    public static function getTitleByType(int $type): ?string
    {
        if(!self::isValidType($type)){
            return null;
        }
        return static::$typeTitleCollection[$type];
    }

    /**
     * Проверяет наличие типа
     *
     * @param $type int
     * @return bool
     */
    public static function isValidType(int $type): bool
    {
        return in_array($type, static::$typeCollection);
    }
}
