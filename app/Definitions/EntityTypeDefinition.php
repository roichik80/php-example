<?php

namespace App\Definitions;

/**
 * Справочник типов основных сущностей, что использует сервис Календаря
 * Class EntityTypeDefinition
 *
 * @package App\Definitions
 * @author Aleksandr Roik
 */
class EntityTypeDefinition extends AbstractDefinition
{
    /**
     * Типы сущностей
     *
     * @const int
     */
    const TASK = 1; // Задача
    const MEETING = 2; // Мероприятие
    const REMINDER = 3; // Напоминание
    const TEXT_NOTE = 4; // Текстовая заметка
    const EVENT_NOTE = 5; // Заметка о событии

    /**
     * Список типов
     *
     * @var array
     */
    private static $typeCollection = [
        self::TASK,
        self::MEETING,
        self::REMINDER,
        self::TEXT_NOTE,
        self::EVENT_NOTE,
    ];

    /**
     * Список публичных типов
     *
     * @var array
     */
    private static $publicTypeCollection = [
        self::TASK,
        self::MEETING,
        self::REMINDER,
    ];

    /**
     * Список названий типов
     *
     * @var array
     */
    private static $typeNameCollection = [
        self::TASK       => 'task',
        self::MEETING    => 'meeting',
        self::REMINDER   => 'reminder',
        self::TEXT_NOTE  => 'textNote',
        self::EVENT_NOTE => 'eventNote',
    ];

    /**
     * Список названий типов
     *
     * @var array
     */
    private static $typeTitleCollection = [
        self::TASK       => 'Задача',
        self::MEETING    => 'Мероприятие',
        self::REMINDER   => 'Напоминание',
        self::TEXT_NOTE  => 'Текстовая заметка',
        self::EVENT_NOTE => 'Заметка о событии',
    ];

    /**
     * Возвращает список типов сущностей
     *
     * @return array
     */
    public static function getTypeCollection(): array
    {
        return self::$typeCollection;
    }

    /**
     * Возвращает список публичных типов
     *
     * @return array
     */
    public static function getPublicTypeCollection(): array
    {
        return self::$publicTypeCollection;
    }

    /**
     * Возвращает список публичных названий типов
     *
     * @return array
     */
    public static function getPublicTypeTitleCollection(): array
    {
        return array_filter(self::$typeTitleCollection, function($v, $k){
            return in_array($k, self::$publicTypeCollection);
        }, ARRAY_FILTER_USE_BOTH);
    }


    /**
     * Возвращает список названий типов сущностей
     *
     * @return array
     */
    public static function getTypeNameCollection(): array
    {
        return self::$typeNameCollection;
    }

    /**
     * Возвращает название сущности по ее типу
     *
     * @param $type int
     * @return string|null
     */
    public static function getTypeName(int $type): ?string
    {
        if (static::isValidType($type)) {
            return static::$typeNameCollection[$type];
        }

        return null;
    }

    /**
     * Проверяет наличие типа
     *
     * @param $type int
     * @return bool
     */
    public static function isValidType(int $type): bool
    {
        return in_array($type, static::$typeCollection);
    }
}
