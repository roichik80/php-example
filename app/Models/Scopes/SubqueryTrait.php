<?php

namespace App\Models\Scopes;

use Illuminate\Support\Facades\DB;
use MaksimM\SubqueryMagic\SubqueryMagic;

/**
 * Общий набор скоупов.
 * Содержит методы, что добавляю субусловия к запросам
 * Trait SubqueryTrait
 *
 * @package App\Models\Scopes
 * @author Aleksandr Roik
 */
trait SubqueryTrait
{
    use SubqueryMagic;

    /**
     * Добавляет сортировку
     *
     * @param $query
     * @param $orders
     */
    private function addOrder($query, $orders)
    {
        if ($orders) {
            foreach ((array)$orders as $order) {
                $order = explode(' ', $order);

                if ($order[0] == 'calendar_id') {
                    $order[0] = 'id';
                }

                if (count($order) >= 2) {
                    $query->orderBy($order[0], $order[1]);
                } else {
                    $query->orderBy($order[0]);
                }
            }
        } else {
            $query->orderBy('begin_at');
        }
    }

    /**
     * Добавляет подзапросы для отбора данных по количеству писем на определенный день
     * Возможно указать лимит на вывод: $limit, $offset
     *
     * @param $query
     * @param int|null $limit
     * @param int|null $offset
     * @param array|null $orders
     * @return mixed
     */
    public function scopeSubqueryValuesByDisplayAt($query, int $limit = null, int $offset = null, $orders = null)
    {
        if ($limit && $offset) {
            $limit += $offset;
        }

        // Отбираем данные
        $query = (__CLASS__)::fromSubquery($query, DB::raw('data, (SELECT @session_id := null, @I := 0) as T'))
            ->selectRaw('*, 
            IF(@session_id = DATE_FORMAT(begin_at, "%Y-%m-%d"), @I := @I + 1, @I := 1) as N,
            @session_id := DATE_FORMAT(begin_at, "%Y-%m-%d") as session_id'
            );

        $this->addOrder($query, $orders);

        $query = (__CLASS__)::fromSubquery(
            $query,
            'data'
        );

        if ($limit) {
            $query->where('N', '<=', $limit);
        }
        if ($offset) {
            $query->where('N', '>=', $offset);
        }

        return $query;
    }

    /**
     * Добавляет подзапросы для отбора данных по количеству писем на определенный день
     * с группировкой по часовой метке, что возвращает функция БД - getTimeSliceByDateTime()
     * Возможно указать лимит на вывод: $limit, $offset
     *
     * @param $query
     * @param int|null $limit
     * @param int|null $offset
     * @param null $orders
     * @return mixed
     */
    public function scopeSubqueryValuesTimeSliceByDisplayAt($query, int $limit = null, int $offset = null, $orders = null)
    {
        if ($limit && $offset) {
            $limit += $offset;
        }

        // Отбираем данные
        $query = (__CLASS__)::fromSubquery($query, DB::raw('data, (SELECT @session_id := null, @I := 0) as T'))
            ->selectRaw('*, 
                IF(@session_id = getTimeSlice15MinutesByDateTime(begin_at), @I := @I + 1, @I := 1) as N,
                @session_id := getTimeSlice15MinutesByDateTime(begin_at) as session_id'
            );

        $this->addOrder($query, $orders);

        $query = (__CLASS__)::fromSubquery(
            $query,
            'data'
        );

        if ($limit) {
            $query->where('N', '<=', $limit);
        }
        if ($offset) {
            $query->where('N', '>=', $offset);
        }

        return $query;
    }

    /**
     * Добавляет подзапрос, в результате которого воздвращается результат ссумированных
     * сущностей в группировкой по динамичному параметру session_id
     *
     * @param $query
     * @return mixed
     */
    public function scopeCountsByDisplayAt($query)
    {
        $query
            ->selectRaw('session_id, count(*) as count_rows')
            ->groupBy('session_id');

        return $query;
    }

}
