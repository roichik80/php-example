<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

/**
 * Набор скоупов для модели Task
 *
 * Trait TaskTrait
 *
 * @package App\Models\Scopes
 * @author Aleksandr Roik
 */
trait TaskTrait
{
    /**
     * Добавляет к запроссу данные перенесенной сущности
     *
     * @param Builder $query
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function scopeParentEntity(Builder $query)
    {
        return $query
            ->addSelect([
                'tasks.*',
                't1.id as children_id',
                't1.begin_at as begin_at_moved',
                't1.end_at as end_at_moved',
            ])
            ->leftJoin('tasks as t1', 't1.parent_id', '=', 'tasks.id');
    }
}
