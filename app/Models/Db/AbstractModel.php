<?php

namespace App\Models\Db;

use App\Database\Eloquent\Builder;
use App\Models\Scopes\SubqueryTrait;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * Базовая абстрактная модель
 *
 * Class AbstractModel
 *
 * @package App\Models\Db
 * @author Aleksandr Roik
 */
abstract class AbstractModel extends Model
{
    use Filterable, SubqueryTrait;

    /**
     * Устанавливает набор атрибутов
     *
     * @param array $attributes
     * @return $this
     */
    public function setAttributes(array $attributes){
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }

        return $this;
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }
}
