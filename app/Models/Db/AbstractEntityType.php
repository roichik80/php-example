<?php

namespace App\Models\Db;

use App\Collections\EntityTypeCollection;

/**
 * Абстрактный класс для моделей типов сущностей
 * Class AbstractEntityTypeModel
 *
 * @package App\Models\Db
 * @author Aleksandr Roik
 * @method static \Illuminate\Database\Eloquent\Builder|self latest($column = 'created_at')
 * @method static \Illuminate\Database\Eloquent\Builder|self find($id, $columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|self create(array $attributes = [])
 */
abstract class AbstractEntityType extends AbstractModel implements EntityTypeInterface
{
    /**
     * Список полей, что надо возвратить для сущности.
     * Данные могут прийти из фронта
     * Если значение is_bool($responseFields) будут возвращатся все разрешенные поля
     * В результате происходи сравнение с массивом $allowedResponseFields
     *
     * @var array|bool|null
     */
    protected $responseFields;

    /**
     * Список разрешенных полей, значения которых могут возвращаться для внешней сущности
     * Если $allowedResponseFields === null - развешены все поля
     *
     * @var array|null
     */
    protected $allowedResponseFields;

    /**
     * Список полей, по которых будет возвращатся результат по умолчанию для Календаря
     *
     * @var array
     */
    protected $responseFieldsDefaultByCalendar = [];

    /**
     * Создание экземпляра новой Eloquent коллекции.
     *
     * @param  array $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new EntityTypeCollection($models);
    }

    /**
     * Связь с сущностью календаря
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function calendars()
    {
        return $this->morphMany(CalendarEntity::class, 'entity');
    }

    /**
     * Установка списка полей, что надо возвратить для сущности
     *
     * @param $responseFields
     * @return $this
     */
    public function setResponseFields($responseFields): self
    {
        $this->responseFields = $responseFields;

        return $this;
    }

    /**
     * Возвращает статус проверки поля на возврат его значения для всешних источников
     *
     * @param $fieldName
     * @return bool
     */
    public function allowedResponseFields($fieldName): bool
    {
        if ($this->allowedResponseFields === null) {
            return true;
        }

        return in_array($fieldName, $this->allowedResponseFields);
    }

    /**
     * Возвращает данные сущности для календаря
     *
     * @return array
     */
    public function getDataByCalendar(): array
    {
        if ($this->responseFields) {
            if (is_bool($this->responseFields)) {
                $data = $this->getDataByFields($this->allowedResponseFields);
            } else {
                $data = $this->getDataByFields($this->responseFields);
            }
        } else {
            $data = $this->getDataByFields($this->responseFieldsDefaultByCalendar);
        }

        return $data;
    }

    /**
     * Возвращает все данные сущности
     *
     * @return array
     */
    public function getData(): array
    {
        if ($this->responseFields) {
            if (is_bool($this->responseFields)) {
                $data = $this->getDataByFields($this->allowedResponseFields);
            } else {
                $data = $this->getDataByFields($this->responseFields);
            }
        } else {
            $data = $this->getDataByFields($this->allowedResponseFields);
        }

        return $data;
    }

    /**
     * Возвращает данные по списку полей
     *
     * @param array $fields
     * @return array
     */
    protected function getDataByFields(array $fields): array
    {
        $data = [];

        if (!$fields) {
            return $data;
        }

        foreach ($fields as $fieldName) {
            if (!$this->allowedResponseFields($fieldName)) {
                continue;
            }

            $data[$fieldName] = $this->getDataByFieldName($fieldName);
        }

        return $data;
    }
}
