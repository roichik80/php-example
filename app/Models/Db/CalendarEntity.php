<?php

namespace App\Models\Db;

use App\Collections\CalendarEntityCollection;
use App\Definitions\EntityTypeDefinition;

/**
 * Модель для сведенных сущностей календаря
 * Class CalendarEntity
 *
 * @property integer $id
 * @property int $company_id
 * @property int $user_id
 * @property int $entity_id
 * @property int $entity_type
 * @property string $begin_at
 * @property string $end_at
 * @property string $filter_hash
 * @method static \Illuminate\Database\Eloquent\Builder|self filter($input = [], $filter = null)
 */
class CalendarEntity extends AbstractModel
{
    /**
     * Определяет необходимость отметок времени для модели
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var
     */
    private $responseFields;

    /**
     * Список разрешенных полей, значения которых могут возвращаться для внешней сущности
     * @var array|null
     */
    protected $allowedResponseFields = [
        'calendar_id',
        'entity_id',
        'entity_type',
        'begin_at',
        'end_at',
    ];


    /**
     * @param mixed $responseFields
     * @return CalendarEntityCollection
     */
    public function setResponseFields($responseFields): self
    {
        $this->responseFields = $responseFields;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponseFieldsByEntityType(int $entityType)
    {
        if($this->responseFields && array_key_exists($entityType, $this->responseFields)) {
            $fields = $this->responseFields[$entityType];

            if(is_string($fields)){
                $fields = json_decode($fields, true);
            }

            return $fields;
        }

        return null;
    }

    /**
     * Возвращает сущность, связанную полиморфной связью
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function entity()
    {
        return $this->morphTo();
    }

    /**
     * Список участников
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function participants()
    {
        return $this->belongsTo(Participant::class, 'entity_id', 'entity_id')
            ->where($this->getTable() . '.entity_type', 'participants.entity_type');
    }

    /**
     * Возвращает связанную сущность типа Задача
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function task()
    {
        return $this->belongsTo(Task::class, 'entity_id')
            ->where($this->getTable() . '.entity_type', EntityTypeDefinition::TASK);
    }

    /**
     * Возвращает связанную сущность типа Напоминание
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reminder()
    {
        return $this->belongsTo(Reminder::class, 'entity_id')
            ->where($this->getTable() . '.entity_type', EntityTypeDefinition::REMINDER);
    }

    /**
     * Возвращает связанную сущность типа Мероприятие
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function meeting()
    {
        return $this->belongsTo(Meeting::class, 'entity_id')
            ->whereEntityType(EntityTypeDefinition::MEETING);
    }

    /**
     * Создаем экземпляр новой Eloquent коллекции
     *
     * @param  array $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new CalendarEntityCollection($models);
    }

    /**
     * Возвращает данные сущности
     *
     * @param bool $appendEntity
     * @return array
     */
    public function getData(bool $appendEntity = true): array
    {
        $data = [
            'calendar_id' => $this->getKey(),
            'entity_id'   => $this->entity_id,
            'entity_type' => $this->entity_type,
        ];

        if ($appendEntity) {
            $data = array_merge(
                $data,
                $this->getEntityDataByCalendar()
            );
        }

        return $data;
    }

    /**
     * @return mixed
     */
    public function getEntityDataByCalendar()
    {
        $entity = $this->entity;

        return $entity
            ->setResponseFields($this->getResponseFieldsByEntityType($entity->getMyEntityType()))
            ->getDataByCalendar();
    }

}
