<?php

namespace App\Models\Filters;

use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Абстрактный класс для фильтров сущностей
 * Class AbstractFilter
 *
 * @package App\Models\Filters
 * @author Alexey Yermakov slims.alex@gmail.com
 */
abstract class AbstractFilter extends ModelFilter
{
    /**
     * AbstractFilter constructor.
     *
     * @param $query
     * @param array $input
     * @param bool $relationsEnabled
     */
    public function __construct($query, array $input = [], $relationsEnabled = true)
    {
        parent::__construct($query, $input, $relationsEnabled);

        $this->init();
    }

    /**
     * Дополнительная инициализаци
     */
    protected function init()
    {
    }

    /**
     * Добавляет новое условие или добавляет к существующему через оператор AND
     * Для добавление нужно передать первым параметром класс конструктора Builder
     *
     * @param Builder $query
     * @param \Closure $callback
     * @param string $operation: and|or. Оператор, что будет обрабатывать замыкание, если $query не будет передан
     */
    protected function addCondition(Builder $query, \Closure $callback, $operation = 'and'): void
    {
        if ($query) {
            $callback($query);
        } else {
            if ($operation == 'and') {
                $this->where($callback, null);
            } elseif ($operation == 'or') {
                $this->orWhere($callback, null);
            }
        }
    }

    /**
     * Создает новое подусловие через оператор AND
     *
     * @param Builder $query
     * @param $values
     */
    protected function andCollection(Builder $query, $values)
    {
        if ($values == false) {
            return;
        }
        foreach ($values as $key => $val) {
            // Call all local methods on filter
            $method = $this->getFilterMethod($key);

            if ($this->methodIsCallable($method)) {
                $this->addCondition($query, function ($query) use ($method, $val) {
                    $query->orWhere(function ($query) use ($method, $val) {
                        $this->{$method}($query, $val);
                    });
                });
            }
        }
    }

    /**
     * Создает новое подусловие через оператор OR
     *
     * @param Builder $query
     * @param $values
     */
    protected function orCollection(Builder $query, $values)
    {
        if ($values == false) {
            return;
        }

        $this->addCondition($query, function ($query) use ($values) {
            $query->where(function ($query) use ($values) {
                foreach ($values as $key => $val) {
                    // Call all local methods on filter
                    $method = $this->getFilterMethod($key);

                    if ($this->methodIsCallable($method)) {
                        $query->orWhere(function ($query) use ($method, $val) {
                            $this->{$method}($query, $val);
                        });
                    }
                }
            });
        });
    }

    /**
     * Обработка условия Where AND
     *
     * @param Builder $query
     * @param array $values
     * @return ModelFilter|void
     */
    public function where(Builder $query, $values)
    {
        if ($values == false) {
            return;
        }

        $this->addCondition($query, function ($query) use ($values) {
            $values = (array)$values;

            foreach ($values as $fieldName => $value) {
                $query->where($fieldName, $value);
            }
        });
    }

    /**
     * Обработка условия Where OR
     *
     * @param Builder $query
     * @param array $values
     * @return ModelFilter|void
     */
    public function orWhere(Builder $query, $values)
    {
        if ($values == false) {
            return;
        }

        $this->addCondition($query, function ($query) use ($values) {
            $values = (array)$values;

            foreach ($values as $fieldName => $value) {
                if (is_array($value)) {
                    $query->orWhereIn($fieldName, $value);
                } else {
                    $query->orWhere($fieldName, $value);
                }
            }
        });
    }

    /**
     * Обработка условия Where between
     *
     * @param Builder $query
     * @param array $values
     * @return ModelFilter|void
     */
    public function whereBetween(Builder $query, $values)
    {
        if ($values == false) {
            return;
        }

        $this->addCondition($query, function ($query) use ($values) {
            $values = (array)$values;

            foreach ($values as $fieldName => $value) {
                $query->whereBetween($fieldName, $value);
            }
        });
    }

    /**
     * Обработка условия Where in
     *
     * @param Builder $query
     * @param array $values
     * @return ModelFilter|void
     */
    public function whereIn(Builder $query, $values)
    {
        if ($values == false) {
            return;
        }

        $this->addCondition($query, function ($query) use ($values) {
            $values = (array)$values;

            foreach ($values as $fieldName => $value) {
                $query->whereIn($fieldName, $value);
            }
        });
    }

    /**
     * Применяет указанный скоуп с параметрами
     *
     * @param string $name
     * @param array ...$args scope arguments
     * @return static
     */
    protected function applyScope($name, ...$args): self
    {
        return $this->scopes([$name => $args]);
    }

    /**
     * Get input to pass to a related Model's Filter.
     *
     * @param string $related
     * @return array
     */
    public function getRelatedFilterInput($related): array
    {
        $inputs = [];

        if ($relations = ($this->relations[$related] ?? [])) {
            foreach ($relations as $alias => $name) {
                if ($value = ($this->input[\is_string($alias) ? $alias : $name] ?? null)) {
                    $inputs[$name] = $value;
                }
            }
        }

        return $inputs;
    }

    /**
     * Filter with input array.
     */
    public function filterInput()
    {
        foreach ($this->input as $key => $val) {
            // Call all local methods on filter
            $method = $this->getFilterMethod($key);

            if ($this->methodIsCallable($method)) {
                $this->{$method}($this->query, $val);
            }
        }
    }

    /**
     * Remove empty strings from the input array.
     *
     * @param array $input
     * @return array
     */
    public function removeEmptyInput($input)
    {
        return $input;
    }
}
