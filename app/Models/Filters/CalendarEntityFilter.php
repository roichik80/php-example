<?php
namespace App\Models\Filters;

use App\Definitions\EntityTypeDefinition;
use App\Definitions\StateDefinition;
use App\Facades\User;
use Illuminate\Database\Eloquent\Builder;
use Topnlab\Common\v2\Reference\Calendar\TaskStatusDefinition;

/**
 * Class CalendarFilter
 *
 * @package App\Models\Filters
 * @author Aleksandr Roik
 */
class CalendarEntityFilter extends AbstractEntityTypeFilter
{
    /**
     * Возвращает список фильтров, что применяются по умолчанию
     *
     * @return array|mixed
     */
    protected function getDefaultFilters()
    {
        return [
            function (Builder $query) {
                $query->where('company_id', User::getCompanyId());
            },
            function (Builder $query) {
                $query->where(function (Builder $query) {
                    $query->orWhere('user_id', User::getId());

                    $responseEntityTypes = array_filter([EntityTypeDefinition::TASK, EntityTypeDefinition::MEETING], function ($value) {
                        return (in_array($value, $this->responseEntityTypes) ? true : false);
                    });

                    if (!$responseEntityTypes) {
                        return;
                    }
                    $query->orWhere(function (Builder $query) use ($responseEntityTypes) {
                        $query->whereIn('entity_type', $responseEntityTypes);
                        $query->whereHas('participants', function (Builder $query) {
                            $query->where('user_id', User::getId());
                        });
                    });
                });
            },

            function (Builder $query) {
                if (!$this->responseEntityTypes) {
                    return;
                }
                $query->whereIn('entity_type', $this->responseEntityTypes);
            },
        ];
    }

    /**
     * Фильтр по Состоянию
     *
     * @param Builder $query
     * @param array|string $statusList - Список Статусов
     */
    public function states(Builder $query, $stateList): void
    {
        $statusList = $this->getStatusByStatesAsArray($stateList);

        if (!$statusList) {
            return;
        }

        $query->where(function ($query) use ($statusList) {
            $entityTypeList = array_filter(EntityTypeDefinition::getPublicTypeCollection(), function ($value) {
                return $value === EntityTypeDefinition::TASK || !$this->isEntityType($value) ? false : true;
            });

            // если есть "другие" типы
            if ($entityTypeList) {
                $this->addCondition($query, function ($query) use ($entityTypeList) {
                    $query->whereIn('entity_type', $entityTypeList);
                });
            }

            // если тип Задача
            if ($this->isEntityType(EntityTypeDefinition::TASK)) {
                $this->addCondition($query, function ($query) use ($statusList) {
                    $entityTypeName = EntityTypeDefinition::getTypeName(EntityTypeDefinition::TASK);
                    $query->orWhereHas($entityTypeName, function ($query) use ($statusList) {
                        $query->whereIn('status', $statusList);
                    });
                });
            }
        });
    }

    /**
     * Возвращает список Статусов по состоянию(ях)
     *
     * @param $stateList
     * @return array|null
     */
    private function getStatusByStatesAsArray($stateList): array
    {
        if (!$stateList) {
            return [];
        }

        $statusList = [];

        foreach ((array)$stateList as $state) {
            $status = null;
            switch ($state) {
                case StateDefinition::MOVED:
                    $statusList[] = TaskStatusDefinition::MOVE;
                    break;
                case StateDefinition::OPENED:
                    $statusList[] = TaskStatusDefinition::NEW;
                    break;
                case StateDefinition::COMPLETED:
                    $statusList[] = TaskStatusDefinition::CLOSE;
                    break;
                case StateDefinition::CANCELED:
                    $statusList[] = TaskStatusDefinition::CANCEL;
                    break;
            }
        }

        return $statusList;
    }

}

