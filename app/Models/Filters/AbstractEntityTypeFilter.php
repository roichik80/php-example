<?php

namespace App\Models\Filters;

use App\Definitions\EntityTypeDefinition;
use App\Facades\User;
use App\Services\TreeFilter\TreeFilter;
use Illuminate\Database\Eloquent\Builder;

abstract class AbstractEntityTypeFilter extends AbstractFilter
{
    /**
     * Список типов сущностей, по которым будут
     *
     * @var array
     */
    protected $responseEntityTypes = [];

    /**
     * Возвращает список фильтров, что применяются по умолчанию
     *
     * @return mixed
     */
    abstract protected function getDefaultFilters();

    /**
     * Дополнительная инициализаци
     */
    protected function init()
    {
        $this->responseEntityTypes = EntityTypeDefinition::getPublicTypeCollection();
    }

    /**
     * Устанавливает список типов
     *
     * @param Builder $query
     * @param $entityTypes
     */
    public function responseEntityTypes(Builder $query, array $entityTypes)
    {
        if (!$entityTypes) {
            return;
        }
        $this->responseEntityTypes = $entityTypes;
    }

    /**
     * Проверяет существование типа пусности
     *
     * @param int $entityType
     * @return bool
     */
    public function isEntityType(int $entityType): bool
    {
        if (!$entityType) {
            return false;
        }

        return in_array($entityType, $this->responseEntityTypes);
    }

    /**
     * Применение списка фильтров
     *
     * @param Builder $query
     * @param string|array $filterHashList
     */
    public function filterHashList(Builder $query, $filterHashList = null)
    {
        // default filters
        $this->buildFilter($query, $this->getDefaultFilters());

        // input filters
        $this->buildFilter($query, $filterHashList, 'orWhere');
    }

    /**
     * Обработка списка фильтров
     *
     * @param Builder $query
     * @param $filterList
     */
    protected function buildFilter(Builder $query, $filterList, $operation = 'where'): void
    {
        if (!$filterList) {
            return;
        }
        $query->where(function ($query) use ($filterList, $operation) {
            foreach ($filterList as $filterValue) {
                //callable
                if (is_callable($filterValue)) {
                    $callable = $filterValue;
                    $query->{$operation}(function ($query) use ($callable, $filterValue) {
                        $this->executeCallable($query, $callable);
                    });
                    //string
                } elseif (is_string($filterValue)) {
                    //method
                    if (method_exists($this, $filterValue)) {
                        $this->{$filterValue}();
                        //filter hash
                    } else {
                        $query->{$operation}(function ($query) use ($filterValue) {
                            resolve(TreeFilter::class)->applyConditions(User::getCompanyType(), $filterValue, $query);
                        });
                    }
                }
            }
        });
    }

    /**
     * Применяет функцию скоупа
     *
     * @param array|string $callable
     * @param $attributes
     */
    protected function executeCallable(Builder $query, $callable, $attributes = null)
    {
        foreach ((array)$callable as $call) {
            $call($query, $attributes);
        }
    }

}
