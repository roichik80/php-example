<?php

namespace App\Http\Middleware;

use App\Facades\ConnectorAuth;
use Closure;
use Illuminate\Http\Request;

/**
 * Class CustomConnectorAuth
 *
 * @package App\Http\Middleware
 */
class CustomConnectorAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!ConnectorAuth::checkAccessFromBackend($request)) {
            throw new \RuntimeException('Сломаный ключ');
        }
        return $next($request);
    }
}
