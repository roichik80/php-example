<?php

namespace App\Http\Middleware;

use App\Facades\User;
use Closure;
use Topnlab\Common\v2\Exceptions\ForbiddenException;

/**
 * Class CustomUserAuth
 *
 * @package App\Http\Middleware
 */
class CustomUserAuth
{
    /**
     * Handle an incoming request.
     * Проверка юзера
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws ForbiddenException
     */
    public function handle($request, Closure $next)
    {
        if (!User::checkAuth()) {
            throw new ForbiddenException('Access forbidden for unauthenticated users.');
        }

        return $next($request);
    }
}
