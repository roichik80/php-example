<?php

namespace App\Http\Requests\Calendar;

class UpdateEntityDateTimeRangeRequest
{
    public function rules()
    {
        return [
            'begin_at'    => 'required|date',
            'end_at'      => 'nullable|date',
            'calendar_id' => 'required:integer',
        ];
    }
}
