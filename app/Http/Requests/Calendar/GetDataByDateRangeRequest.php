<?php

namespace App\Http\Requests\Calendar;

use App\Definitions\EntityTypeDefinition;
use App\Definitions\StateDefinition;
use App\Facades\User;
use App\Http\Requests\Request;
use App\Services\TreeFilter\TreeFilter;
use Illuminate\Validation\Rule;

/**
 * Вилидация входящих параметров запроса для действий контроллера CalendarController
 * Class CalendarFilterRequest
 *
 * @package App\Http\Requests
 * @author Aleksandr Roik
 */
class GetDataByDateRangeRequest extends Request
{
    public function rules()
    {
        return [
            'begin_at'       => 'required|date',
            'end_at'         => 'required|date',
            'limit'          => 'required|integer|min:1|max:' . config('general.calendar_entities.max_limit_values.general'),
            'filters'        => ['array', Rule::in(resolve(TreeFilter::class)->getFilterHashes(User::getCompanyType()))],
            'states'         => ['array', Rule::in(StateDefinition::getPublicTypeCollection())],
            'offset'         => 'nullable|integer|min:1',
            'use_time_slice' => 'nullable|in:0,1',
            'fields'         => 'array',
            'entity_types'   => ['array', Rule::in(EntityTypeDefinition::getPublicTypeCollection())],
            'order'          => 'array',
        ];
    }
}
