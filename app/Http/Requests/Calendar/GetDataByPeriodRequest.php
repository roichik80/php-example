<?php

namespace App\Http\Requests\Calendar;

use App\Definitions\EntityTypeDefinition;
use App\Definitions\PeriodDefinition;
use App\Definitions\StateDefinition;
use App\Facades\User;
use App\Http\Requests\Request;
use App\Services\TreeFilter\TreeFilter;
use Illuminate\Validation\Rule;

/**
 * Вилидация входящих параметров запроса для действий контроллера CalendarController
 * Class GetDataByPeriodRequest
 *
 * @package App\Http\Requests
 * @author Aleksandr Roik
 */
class GetDataByPeriodRequest extends Request
{
    public function rules()
    {
        return [
            'period'         => ['required', Rule::in(PeriodDefinition::getPeriodCollection())],
            'date_time'      => 'required|date',
            'limit'          => 'required_if:period,day,week,month,list|integer|min:1|max:' . config('general.calendar_entities.max_limit_values.' . $this->input('period')),
            'filters'        => ['array', Rule::in(resolve(TreeFilter::class)->getFilterHashes(User::getCompanyType()))],
            'states'         => ['array', Rule::in(StateDefinition::getPublicTypeCollection())],
            'offset'         => 'nullable|integer|min:1',
            'use_time_slice' => 'nullable|in:0,1',
            'fields'         => 'array',
            'entity_types'   => ['array', Rule::in(EntityTypeDefinition::getPublicTypeCollection())],
            'order'          => 'array|string',
        ];
    }
}
