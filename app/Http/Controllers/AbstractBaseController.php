<?php

namespace App\Http\Controllers;

/**
 * Базовый контролер клиентского API
 * Class AbstractBaseController
 *
 * @package App\Http\Controllers
 * @author Aleksandr Roik
 */
abstract class AbstractBaseController extends Controller
{
    /**
     * Менеджер модели
     *
     * @var $modelManager
     */
    protected $modelManager;

    /**
     * Возвращает сущность по id
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json($this->modelManager->findAsArray($id));
    }

    /**
     * Удаление сущности по id
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json($this->modelManager->delete($id));
    }
}
