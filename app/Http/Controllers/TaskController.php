<?php

namespace App\Http\Controllers;

use App\Http\Requests\Task\CreateTaskRequest;
use App\Http\Requests\Task\UpdateTaskRequest;
use App\Managers\TaskManager;

/**
 * Контроллер типа сущности "Задача"
 * Class TaskController
 *
 * @package App\Http\Controllers
 * @author Aleksandr Roik
 */
class TaskController extends AbstractEntityController
{
    /**
     * @param TaskManager $modelManager
     */
    public function __construct(TaskManager $modelManager)
    {
        $this->modelManager = $modelManager;
    }

    /**
     * Создание новой сущности
     *
     * @param CreateTaskRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTaskRequest $request)
    {
        return response()->json($this->modelManager->create($request->all()));
    }

    /**
     * Обновление сущности по id
     *
     * @param UpdateTaskRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, $id)
    {
        return response()->json($this->modelManager->update($id, $request->all()));
    }
}
