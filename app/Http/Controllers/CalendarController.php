<?php

namespace App\Http\Controllers;

use App\Definitions\EntityTypeDefinition;
use App\Definitions\PeriodDefinition;
use App\Definitions\StateDefinition;
use App\Facades\User;
use App\Http\Requests\Calendar\GetDataByPeriodRequest;
use App\Http\Requests\Calendar\UpdateEntityDateTimeRangeRequest;
use App\Managers\CalendarEntityManager;
use App\Services\TreeFilter\TreeFilter;
use Illuminate\Http\Request;
use App\Http\Requests\Calendar\GetDataByDateRangeRequest;

/**
 * Контроллер календаря
 * Class CalendarController
 *
 * @package App\Http\Controllers
 * @author Aleksandr Roik
 */
class CalendarController extends AbstractBaseController
{
    /**
     * Возвращает данные сущности по ее ID
     *
     * @param int $calendarId
     * @return array
     */
    public function getDataById(int $calendarId)
    {
        return (new CalendarEntityManager())->findAsArray($calendarId);
    }

    /**
     * Возвращает список сущностей календаря за предложеный период времени
     *
     * @param Request $request
     * @return mixed
     */
    public function getDataByDateTimeRange(GetDataByDateRangeRequest $request)
    {
        $beginAt = $request->input('begin_at');
        $endAt = $request->input('end_at');
        $filters = $request->input('filters', []);
        $states = $request->input('states');
        $limit = $request->input('limit');
        $offset = $request->input('offset', 0);
        $useTimeSlice = (bool)$request->input('use_time_slice', false);
        $responseEntityTypes = $request->input('entity_types', []);
        $responseFields = $request->input('fields', []);
        $order = $request->input('order');

        $result = (new CalendarEntityManager())
            ->setFilters($filters)
            ->setStates($states)
            ->setLimit($limit)
            ->setOffset($offset)
            ->setUseTimeSlice($useTimeSlice)
            ->setResponseEntityTypes($responseEntityTypes)
            ->setResponseFields($responseFields)
            ->setOrder($order)
            ->getDataByDateTimeRangeAsArray($beginAt, $endAt);

        return $result;
    }

    /**
     * Возвращает список сущностей календаря за определенный период
     *
     * @param Request $request
     * @return mixed
     */
    public function getDataByPeriod(GetDataByPeriodRequest $request)
    {
        $period = $request->input('period');
        $dateTime = $request->input('date_time', date('Y-m-d 00:00:00'));
        $filters = $request->input('filters', []);
        $states = $request->input('states');
        $limit = $request->input('limit');
        $offset = $request->input('offset', 0);
        $useTimeSlice = (bool)$request->input('use_time_slice', false);
        $responseEntityTypes = $request->input('entity_types', []);
        $responseFields = $request->input('fields', []);
        $order = $request->input('order');

        $result = (new CalendarEntityManager())
            ->setFilters($filters)
            ->setStates($states)
            ->setLimit($limit)
            ->setOffset($offset)
            ->setUseTimeSlice($useTimeSlice)
            ->setResponseFields($responseEntityTypes)
            ->setResponseFields($responseFields)
            ->setOrder($order)
            ->getDataByPeriodRangeAsArray($period, $dateTime);

        return $result;
    }

    /**
     * Возвращает список количества сущностей календаря за определенный период
     *
     * @param Request $request
     * @return mixed
     */
    public function getDataCountByDateTimeRange(GetDataByDateRangeRequest $request)
    {
        $beginAt = $request->input('begin_at');
        $endAt = $request->input('end_at');
        $filters = $request->input('filters', []);
        $states = $request->input('states');
        $limit = $request->input('limit');
        $offset = $request->input('offset', 0);
        $useTimeSlice = (bool)$request->input('use_time_slice', false);
        $responseEntityTypes = $request->input('entity_types', []);
        $responseFields = $request->input('fields', []);
        $order = $request->input('order');

        $result = (new CalendarEntityManager())
            ->setFilters($filters)
            ->setStates($states)
            ->setLimit($limit)
            ->setOffset($offset)
            ->setUseTimeSlice($useTimeSlice)
            ->setResponseFields($responseEntityTypes)
            ->setResponseFields($responseFields)
            ->setOrder($order)
            ->getDataCountByDateTimeRangeAsArray($beginAt, $endAt);

        return $result;
    }

    /**
     * Возвращает доступных список фильтров
     *
     * @return mixed
     */
    public function getFilterList(TreeFilter $tree)
    {
        return $tree->getFilterTree(User::getCompanyType());
    }

    /**
     * Возвращает доступных список периодов
     *
     * @return mixed
     */
    public function getPeriodList()
    {
        return PeriodDefinition::getPeriodTitleCollection();
    }

    /**
     * Возвращает список состояний
     *
     * @return mixed
     */
    public function getStateList()
    {
        return StateDefinition::getPublicTypeTitleCollection();
    }

    /**
     * Возвращает список типов сущностей
     *
     * @return array
     */
    public function getEntityTypeList()
    {
        return EntityTypeDefinition::getPublicTypeTitleCollection();
    }
}
