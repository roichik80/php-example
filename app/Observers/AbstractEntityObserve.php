<?php

namespace App\Observers;

use App\Facades\User;
use App\Managers\CalendarEntityManager;
use App\Models\Db\AbstractModel;

/**
 * Базовый абстрактный класс для типов
 * Class AbstractEntityObserve
 *
 * @package App\Observers
 * @author Aleksandr Roik
 */
abstract class AbstractEntityObserve
{
    /**
     * Слушатель после создание сущности
     *
     * @param AbstractModel $model
     * @return void
     */
    public function created(AbstractModel $model)
    {
        $this->updateFilterHash($model);
    }

    /**
     * Слушатель после обновления сущности
     *
     * @param AbstractModel $model
     * @return void
     */
    public function updated(AbstractModel $model)
    {
        $this->updateFilterHash($model);
    }

    /**
     * Слушатель перед созданием сущности
     *
     * @param AbstractModel $model
     */
    public function creating(AbstractModel $model)
    {
        $model->setAttribute('user_id', User::getId());
        $model->setAttribute('company_id', User::getCompanyId());
    }

    /**
     * Обновление хэша в сущности calendar_entities
     *
     * @param AbstractModel $model
     */
    protected function updateFilterHash(AbstractModel $model)
    {
        (new CalendarEntityManager())->updateFilterHash(User::getCompanyType(), $model);
    }
}
