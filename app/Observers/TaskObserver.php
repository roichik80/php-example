<?php

namespace App\Observers;

use App\Models\Db\AbstractModel;

/**
 * Наблюдатель для модели Tasks
 * Class TaskObserver
 *
 * @package App\Observers
 * @author Aleksandr Roik
 */
class TaskObserver extends AbstractEntityObserve
{
    use ParticipantEventsTrait, NotificationEventsTrait;

    /**
     * Слушатель после создание сущности
     *
     * @param AbstractModel $model
     * @return void
     */
    public function created(AbstractModel $model)
    {
        parent::created($model);

        $this->createParticipants($model);
        $this->createNotifications($model);
    }

    /**
     * Слушатель после обновления сущности
     *
     * @param AbstractModel $model
     * @return void
     */
    public function updated(AbstractModel $model)
    {
        parent::updated($model);

        $this->updateParticipants($model);
        $this->updateNotifications($model);
    }
}
