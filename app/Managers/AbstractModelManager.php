<?php

namespace App\Managers;

use App\Exceptions\ModelNotDeleteException;
use App\Exceptions\ModelNotSaveException;
use App\Exceptions\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

/**
 * Базовый абстрактный класс для реализации менеджера моделей
 * Class ModelManager
 *
 * @package App\Managers
 * @Autor Aleksandr Roik
 */
abstract class AbstractModelManager
{
    /**
     * Возвращает название класса модели
     *
     * @return string
     */
    abstract public function modelClass(): string;

    /**
     * ModelManager constructor.
     */
    public function __construct()
    {
        $this->initialize();
    }

    /**
     * Заглушка функции инициализации
     */
    public function initialize()
    {
    }

    /**
     * магическая проброска на методы модели
     *
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $modelClass = $this->modelClass();
        if (is_string($modelClass)) {
            $modelClass = new $modelClass();
        }

        if (method_exists($modelClass, $name)) {
            return $modelClass::$name(...$arguments);
        } else {
            throw new \BadMethodCallException("There is no method {$name} in class " . static::class);
        }
    }

    /**
     * Возвращает сущность по id
     *
     * @param int $id
     * @return array
     */
    public function findAsArray(int $id): array
    {
        $model = $this->findOneById($id);

        if ($model) {
            return $model->getData();
        }

        return [];
    }

    /**
     * Поиск одной сущности по id
     *
     * @param int $id
     * @return object|null
     */
    public function findOneById(int $id): ?object
    {
        if ($model = $this->modelClass()::find($id)) {
            return $model;
        }

        throw new ModelNotFoundException(sprintf('Не удалось найти запись с id: %s', $id));
    }

    /**
     * Создание сущности
     *
     * @param array $data
     * @return bool
     */
    public function create(array $data): bool
    {
        $className = $this->modelClass();
        $model = new $className();
        DB::beginTransaction();

        try {
            if ($model->fill($data)->save()) {
                DB::commit();

                return true;
            }
        } catch (\Exception|\Error $e) {
            DB::rollBack();
            if (config('app.debug')) {
                throw new ModelNotSaveException('Не удалось создать запись.' . $e->getMessage());
            }
        }

        throw new ModelNotSaveException('Не удалось создать запись');
    }

    /**
     * Обновление сущности
     *
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update(int $id, array $data): bool
    {
        $model = $this->findOneById($id);
        DB::beginTransaction();

        try {
            if ($model->fill($data)->save()) {
                DB::commit();

                return true;
            }
        } catch (\Exception|\Error $e) {
            DB::rollBack();
            if (config('app.debug')) {
                throw new ModelNotSaveException('Не удалось обновить запись.' . $e->getMessage());
            }
        }

        throw new ModelNotSaveException('Не удалось обновить запись');
    }

    /**
     * Удаление сущности
     *
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        $model = $this->findOneById($id);
        DB::beginTransaction();

        try {
            if ($model->delete()) {
                DB::commit();

                return true;
            }
        } catch (\Exception|\Error $e) {
            DB::rollBack();
            if (config('app.debug')) {
                throw new ModelNotDeleteException('Не удалось удалить запись.' . $e->getMessage());
            }
        }

        throw new ModelNotDeleteException();
    }
}
