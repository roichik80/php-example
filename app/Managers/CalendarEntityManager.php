<?php

namespace App\Managers;

use App\Extensions\DateTimeHelper;
use App\Models\Db\AbstractModel;
use App\Models\Db\CalendarEntity;
use App\Models\Filters\CalendarEntityFilter;
use App\Services\TreeFilter\TreeFilter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Менеджер для модели CalendarEntity
 * Class CalendarEntityManager
 *
 * @package App\Managers
 * @author Aleksandr Roik
 */
class CalendarEntityManager extends AbstractModelManager
{
    /**
     * Список входящих фильтров.
     *
     * @var array
     */
    private $filters;

    /**
     * Список состояний (статусов)
     *
     * @var array|string
     */
    private $states;

    /**
     * Лимитированное количество сущностей, что необходимо возвратить
     *
     * @var int
     */
    private $limit;

    /**
     * Номер сущности, насиная с которой надо созвратить данные
     *
     * @var int
     */
    private $offset;

    /**
     * Указывает на изпользование в запроссах выборки функции отбора и группировки
     * сущностей о временному срезу
     *
     * @var boolean
     */
    private $useTimeSlice = false;

    /**
     * Список типов сущностей, по которым надо возвратить данные
     *
     * @var array
     */
    private $responseEntityTypes = [];

    /**
     * Асоцированный список полей, что будет возвращен для определенной сущности
     * Ключем должен быть Тип сущности, Значение - массив полей Типа сущности
     *
     * @var array
     */
    private $responseFields = [];

    /**
     * Список полей для сортировки
     *
     * @var
     */
    private $order;

    /**
     * Возвращает название класса модели
     *
     * @return string
     */
    public function modelClass(): string
    {
        return CalendarEntity::class;
    }

    /**
     * @param array $filters
     * @return CalendarEntityManager
     */
    public function setFilters(array $filters): self
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * @param array|string $states
     * @return CalendarEntityManager
     */
    public function setStates($states): self
    {
        $this->states = $states;

        return $this;
    }

    /**
     * @param int $limit
     * @return CalendarEntityManager
     */
    public function setLimit(int $limit = null): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @param int $offset
     * @return CalendarEntityManager
     */
    public function setOffset(int $offset = null): self
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @param bool $useTimeSlice
     * @return CalendarEntityManager
     */
    public function setUseTimeSlice(bool $useTimeSlice): self
    {
        $this->useTimeSlice = $useTimeSlice;

        return $this;
    }

    /**
     * @param array $responseFields
     * @return CalendarEntityManager
     */
    public function setResponseFields(array $responseFields): self
    {
        $this->responseFields = $responseFields;

        return $this;
    }

    /**
     * @param array $responseEntityTypes
     * @return CalendarEntityManager
     */
    public function setResponseEntityTypes(array $responseEntityTypes): self
    {
        $this->responseEntityTypes = $responseEntityTypes;

        return $this;
    }

    /**
     * @param mixed $order
     * @return CalendarEntityManager
     */
    public function setOrder($order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Возвращает конструктор запросса для
     *
     * @param string $beginAt
     * @param string $endAt
     * @return Builder
     */
    private function getQueryBuilderByDateRange(string $beginAt, string $endAt): Builder
    {
        // Если не передано время в параметре $endDate - добавляем 23:59:59
        $dateTime = (new \DateTime($endAt));
        if (mb_strlen($endAt) <= 10 || $dateTime->format('His') == false) {
            $endAt = $dateTime->format('Y-m-d 23:59:59');
        }

        // Применяем фильтр
        $className = $this->modelClass();

        return (new $className())
            ->whereBetween('begin_at', [$beginAt, $endAt])
            ->filter(
                ['responseEntityTypes' => $this->responseEntityTypes, 'filterHashList' => $this->filters, 'states' => $this->states],
                CalendarEntityFilter::class
            );
    }

    /**
     * Возвращает список сущностей за указанный период времени
     *
     * @param string $beginAt
     * @param string $endAt
     * @return array
     */
    public function getDataByDateTimeRangeAsArray(string $beginAt, string $endAt): array
    {
        $query = $this->getQueryBuilderByDateRange($beginAt, $endAt);

        if ($this->useTimeSlice) {
            $query = $query->subqueryValuesTimeSliceByDisplayAt($this->limit, $this->offset, $this->order);
        } else {
            $query = $query->subqueryValuesByDisplayAt($this->limit, $this->offset, $this->order);
        }

        $collect = $query
            ->get()
            ->load('entity');

        if ($collect->isEmpty() == false) {
            return $collect
                ->setResponseFields($this->responseFields)
                ->getAllAsArray();
        }

        return [];
    }

    /**
     * Возвращает список сущностей за период
     *
     * @param string $period
     * @param string $dateTime
     * @return array
     */
    public function getDataByPeriodRangeAsArray(string $period, string $dateTime): array
    {
        $dateRange = DateTimeHelper::getDateTimeRangeByPeriod($period, $dateTime);
        $query = $this->getQueryBuilderByDateRange($dateRange['beginAt'], $dateRange['endAt']);

        if ($this->useTimeSlice) {
            $query = $query->subqueryValuesTimeSliceByDisplayAt($this->limit, $this->offset);
        } else {
            $query = $query->subqueryValuesByDisplayAt($this->limit, $this->offset);
        }

        $collect = $query
            ->get()
            ->load('entity');

        if ($collect->isEmpty() == false) {
            return $collect
                ->setResponseFields($this->responseFields)
                ->getAllAsArray();
        }

        return [];
    }

    /**
     * Возвращает количество сущностей календаря c групировкой по полю displayAt
     *
     * @param string $beginAt
     * @param string $endAt
     * @return array
     */
    public function getDataCountByDateTimeRangeAsArray(string $beginAt, string $endAt): array
    {
        $query = $this->getQueryBuilderByDateRange($beginAt, $endAt);

        if ($this->useTimeSlice) {
            $query = $query->subqueryValuesTimeSliceByDisplayAt($this->limit, $this->offset);
        } else {
            $query = $query->subqueryValuesByDisplayAt($this->limit, $this->offset);
        }

        $collect = $query
            ->countsByDisplayAt()
            ->get()
            ->load('entity');

        if ($collect->isEmpty() == false) {
            return $collect
                ->setResponseFields($this->responseFields)
                ->getCountsByDateRangeAsArray();
        }

        return [];
    }

    /**
     * Обновление хэша
     *
     * @param int $companyType
     * @param AbstractModel $model
     */
    public function updateFilterHash(int $companyType, AbstractModel $model): void
    {
        $filterHash = resolve(TreeFilter::class)->getFilterHash($companyType, $model);

        $entityData = (new CalendarEntity())
            ->where('entity_id', $model->getKey())
            ->where('entity_type', $model->getMyEntityType())
            ->first();

        if (!$entityData) {
            return;
        }

        $entityData->filter_hash = $filterHash;
        $entityData->save();
    }
}
