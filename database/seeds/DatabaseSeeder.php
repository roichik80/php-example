<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CustomTypeTaskTableSeeder::class,
            PostCardsTableSeeder::class,
            TasksTableSeeder::class,
            MeetingsTableSeeder::class,
            RemindersTableSeeder::class,
            TextNotesTableSeeder::class,
            EventNotesTableSeeder::class,
        ]);
    }
}
