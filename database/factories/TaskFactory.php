<?php

use Faker\Generator as Faker;
use Topnlab\Common\v2\Reference\Secondary\Realty\RealtyActionTypeDefinition;
use App\Definitions\ObjectStatusDefinition;
use Topnlab\Common\v2\Reference\Calendar\OwnerTypeDefinition;
use Topnlab\Common\v2\Reference\Calendar\TaskSubtypeDefinition;
use Topnlab\Common\v2\Reference\Calendar\TaskStatusDefinition;
use App\Models\Db\CustomTaskType;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
    
$factory->define(\App\Models\Db\Task::class, function (Faker $faker) {
    static $custom_task_type_id_list;

    if($custom_task_type_id_list === null) {
        $collection = CustomTaskType::where('company_id', '1')->get();
        $custom_task_type_id_list = $collection->pluck('id')->all();
    }

    $beginAt = $faker->dateTimeBetween('-6 month', '+6 month');
    $endAt = $beginAt;

    return [
        'begin_at'            => $beginAt,
        'end_at'              => $endAt,
        'subtype'             => $faker->randomElement(TaskSubtypeDefinition::getTypeCollection()),
        'owner_id'            => $faker->numberBetween(1, 20),
        'owner_type'          => $faker->randomElement(OwnerTypeDefinition::getTypeCollection()),
        'complex_id'          => $faker->numberBetween(1, 100),
        'description'         => 'Описание ' . $faker->numberBetween(1, 100),
        'custom_task_type_id' => $faker->randomElement($custom_task_type_id_list),
        'object_status'       => $faker->randomElement(ObjectStatusDefinition::getStatusCollection()),
        'object_action'       => $faker->randomElement([RealtyActionTypeDefinition::REALTY_ACTION_TYPE_RENT, RealtyActionTypeDefinition::REALTY_ACTION_TYPE_SALE]),
        'parent_id'           => $faker->numberBetween(1, 1000),
        'status'              => $faker->randomElement(TaskStatusDefinition::getStatusCollection()),
        'comment'             => 'Комментарий  ' . $faker->numberBetween(1, 100),
        'expiration_at'       => null,
    ];
});
